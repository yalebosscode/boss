mainprogram

DFT+Subsidiary-Particle Solver
Started on 16-Feb-2021 00:21:11
Verbosity level verbose = 0

+++ Setting up physical system
Correlated orbitals: corbs =      1     4
Uncorrelated orbitals: porbs =      2     3     5     6     7     8     9    10    11    12    13    14
Total number of orbitals: norbs = 14
Will read hrbinfileup=LNO.bin and hrbinfiledn=LNO.bin with tijtol = 0 eV and zeroddflag = 0
occtol = 1e-05  occtol_fac = 1000
K-point sampling: dim=3  nk=10  kTspinon=0.1 eV  Ne=25  Ntol=1e-08  Dpd=0 eV
nsites = 1
nsubs/site = 4
ncorbs/sub = 1
spinresolved = 1
orbresolved = 1
Allowed occupancies per subsidiary =      0     1
U (eV) =     12
J (eV) =      1
Uprime (eV) =     10
Oavgtol = 1e-08  kTsub = 0.1 eV
dh = 0.001    dC = 0.001
Subsidiary Jacobian computed every niter_calc_dndh=5 steps
Setting up subsidiary statetable information:
  isite=1  isub=1  table(corb,spin) =
     1     1
  isite=1  isub=2  table(corb,spin) =
     1     2
  isite=1  isub=3  table(corb,spin) =
     2     1
  isite=1  isub=4  table(corb,spin) =
     2     2
Site indices for corbs:
 site(1:ncorbs) =      1     1
Spin resolution of subsidiaries:
  isite=1  spin=1 : local sub indices =      1     3
  isite=1  spin=2 : local sub indices =      2     4
Orbital resolution of subsidiaries:
  isite=1 : localicorb=1, local subsidiary indices =      1     2
  isite=1 : localicorb=2, local subsidiary indices =      3     4
Initial C all set to 1.  C(site,sub) =
     1     1     1     1
Initial Oavg(spin,corb) =
     1     1
     1     1
Initital h(spin,corb) =
     0     0
     0     0
Reading wannier data from LNO.bin for spin up
--- inputhrbin() ---
Reading binary file LNO.bin
  orbs =      1     4     2     3     5     6     7     8     9    10    11    12    13    14
  corbs =      1     4
  site_of_corb =      1     1
  tijtol = 0 eV
  Opening file LNO.bin
  tRij has 24500 entries ; norbs=14 ; 24500/14^2 = 125
  Found nR = 125 unique R vectors
  Maximum imaginary tRij = 0 eV ; zeroing Im(tRij)
  Removing hoppings below tijtol=0 eV (absolute value)
  Now have 24500 hopping entries
  Found nR = 125 unique R vectors
Reading wannier data from LNO.bin for spin dn
--- inputhrbin() ---
Reading binary file LNO.bin
  orbs =      1     4     2     3     5     6     7     8     9    10    11    12    13    14
  corbs =      1     4
  site_of_corb =      1     1
  tijtol = 0 eV
  Opening file LNO.bin
  tRij has 24500 entries ; norbs=14 ; 24500/14^2 = 125
  Found nR = 125 unique R vectors
  Maximum imaginary tRij = 0 eV ; zeroing Im(tRij)
  Removing hoppings below tijtol=0 eV (absolute value)
  Now have 24500 hopping entries
  Found nR = 125 unique R vectors
--- create_spinon_kgrid:
Creating dim=3 spinon kgrid of density 10 x 10 x 10

+++ Setting Bfield=0
+++ Solving U=Up=J=0 & Bfield=0 problem to initialize
*** Special case!  t->t/2 due to length(allowedOccs)==2
*** Special case!  t->t/2 due to length(allowedOccs)==2
dcount0(spin,corb) =
      0.52889      0.52889
      0.52889      0.52889

+++ Adding possible symmetry breaking fields
Bfield = 
     0     0
     0     0

+++ Calling main SCF loop
Desired occtol = 1e-05  ;  Subsidiary problem solved to occtol = 1e-08 in SCF loop
*** Special case!  t->t/2 due to length(allowedOccs)==2
*** Special case!  t->t/2 due to length(allowedOccs)==2
SCF step   1 : Etot= 258.30428539
*** Special case!  t->t/2 due to length(allowedOccs)==2
*** Special case!  t->t/2 due to length(allowedOccs)==2
SCF step   2 : Etot= 258.40998891   dE= +1.1e-01   max|Delta(dcount)|= 1.1e-03   occtol= 1.0e-05
*** Special case!  t->t/2 due to length(allowedOccs)==2
*** Special case!  t->t/2 due to length(allowedOccs)==2
SCF step   3 : Etot= 258.40549518   dE= -4.5e-03   max|Delta(dcount)|= 1.5e-04   occtol= 1.0e-05
*** Special case!  t->t/2 due to length(allowedOccs)==2
*** Special case!  t->t/2 due to length(allowedOccs)==2
SCF step   4 : Etot= 258.40609297   dE= +6.0e-04   max|Delta(dcount)|= 2.1e-05   occtol= 1.0e-05
*** Special case!  t->t/2 due to length(allowedOccs)==2
*** Special case!  t->t/2 due to length(allowedOccs)==2
SCF step   5 : Etot= 258.40601107   dE= -8.2e-05   max|Delta(dcount)|= 2.8e-06   occtol= 1.0e-05
++++ Main SCF loop converged!
Final dcount =
      0.55197      0.55198
      0.55197      0.55198
Final Oavg = 
      0.67166      0.67166
      0.67166      0.67166
Etot = 258.40601107   Eband = 257.44129263   Eint = 0.96471843

+++ Plotting bands and PDOS

getvf
v0 =
     0.060138
vs =
     0.030266
ans =
        1.987
diary off
