
This directory has example output from running the BoSS code on
LaNiO3: a single formula unit with a 5-atom unit cell.  The Wannier90
v1.2 lno_hr.dat.gz is provided for this system which was obtained from
a standard DFT calculation with Quantum Espresso.  The file should be
converted to binary format as in the other examples using the
convert_hrdat_to_bin function.

The setup_system.m file represents a specific calculation with U=10,
J=3, U'=U-2J=4 eV and with a orbital only subsidiary model for the eg
orbitals of Ni (all other orbitals which are O 2p and Ni t2g are
uncorrelated): thus here are two subsidiaries on the Ni site each with
occupancy going from 0:2.  This is accomplished by the code lines
% Number of subsidiary modes per site
subinfo.nsubspersite = 2;
% Allowed occupancies for each subsidiary mode
subinfo.allowedOccs = [0:2]; 
% Number of corr. orbitals per subsidiary mode
subinfo.ncorbspersub = 1; 
% Whether subsidiaries have spin index (1) or are summed over spin (0)
subinfo.spinresolved = 0;

This is to be compared to a spin+orbital subsidiary model which is given by
% Number of subsidiary modes per site
subinfo.nsubspersite = 4;
% Allowed occupancies for each subsidiary mode
subinfo.allowedOccs = [0:1]; 
% Number of corr. orbitals per subsidiary mode
subinfo.ncorbspersub = 1; 
% Whether subsidiaries have spin index (1) or are summed over spin (0)
subinfo.spinresolved = 1;

The idea of this example is to compare the two subsidiary models for a
range of (U,J) values (and U'=U-2J throughout) to see how they
differ. The directory has a set of output log files from all the
various runs that were done: these then generate the data in the main
documentation file (as well as the main paper for this code).  The
utility program "getvf.m" is to be run after "mainprogram.m" is
finished: is computes the bands from Gamma to X, plots them, finds the
crossing of the highest band (dx2-y2 character) across the Fermi
energies and extracts the slopes to get velocities, then takes a
ratio of the velocities without and with interaction to get the "mass
enhancement factor".

If you want to see the full ground state subsidiary wave function and
probabilities printed out, turn "verbose" on (set to 1) in the
"mainprogram.m" and look at the very end of the long output for the
final iteration.

