% Recompute bare spinon bands and scaled spinon bands to get their
% respective chemical potentials mu1 and mu
O1 = ones(size(Oavg));
[eklist1,vklist1,rho1,mu1,tRijscale1,Eband1] = solvespinon_fixedN(...,
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,O1);
clear O1 rho1 Eband1
[eklist,vklist,rho,mu,tRijscale,Eband] = solvespinon_fixedN(...,
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,Oavg);

kl = linspace(0,0.5,50)'*[1 0 0];
kl = kl(:,1:3);
kaplot = kl*2*pi;
         
% compute band energies along the path
nkatotplot = size(kaplot,1);
ekplot1 = zeros(nkatotplot,norbs,2);
ekplot = ekplot1;
for j=1:nkatotplot
    for ispin=1:2
        [vk,ek1] = diagH_kspinon(kaplot(j,:)',Rlist,tRijscale1(:,:,:,ispin));
        [vk,ek] = diagH_kspinon(kaplot(j,:)',Rlist,tRijscale(:,:,:,ispin));
        ekplot1(j,:,ispin) = ek1;
        ekplot(j,:,ispin) = ek;
    end
end
ekplot1 = ekplot1 - mu1;
ekplot = ekplot - mu;

% plot the bands for each spin separately
figure(3)
clf
minmin = min([min(ekplot1(:)) min(ekplot(:))]);
maxmax = max([max(ekplot1(:)) max(ekplot(:))]);
ispin=1;
p1 = plot(ekplot1(:,:,ispin),'g-.','linewidth',2);
hold on
ps = plot(ekplot(:,:,ispin),'r-','linewidth',3);
set(gca,'fontsize',16)
title(sprintf('Bare+spinon bands'))
xlabel('k')
if ispin == 1
    ylabel('E_{nk} - \mu (eV)')
    legend([p1(1),ps(1)],'bare','spinon','location','northwest')
end
grid on
set(gca,'xtick',[1 26 51])
set(gca,'xticklabel',{'R','\Gamma','M'})
axis([1 50 -1 1])


idx1 = find(ekplot1(1:end-1,14,ispin).*ekplot1(2:end,14,ispin)<0);
idx = find(ekplot(1:end-1,14,ispin).*ekplot(2:end,14,ispin)<0);
if length(idx1)~=1 || length(idx)~=1
    stop
end

v0 = ekplot1(idx1+1,14,ispin)-ekplot1(idx1,14,ispin)
vs = ekplot(idx+1,14,ispin)-ekplot(idx,14,ispin)
v0/vs



