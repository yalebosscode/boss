
This directory contains an example of a manually created file for a
tight-binding model written "by hand" to realize a simple p-d model.  The
point is that one can make a p-d tight-binding model easily
as input for the subsidiary-boson calculation without a previous electronic
structure calculation.

This particular model is as simple as you can imagine it: there is one
d and one p orbital per unit cell.  Physically, imagine a linear
chain of equaly space oxygen and metal atoms along the z axis; the
Hilbert space is spanned by the O 2pz and metal 3z2-r2 space.  Spin up
and down are equivalent.  The O 2pz has on site energy -3 eV, the
metal 3z2-r2 has on site energy 0 eV, and the hopping between 2pz and
3z2-r2 is +1.0 or -1.0 eV (depending on sign convention but whatever
sign you choose, you must be consistent about hopping along +z or -z
as the 2pz is odd and 3z2-r2 is even along z).  Only nearest neighbor
hopping is allowed.

If you look athe provided "manual_hr.dat" file you can see its simple
structure:
line 1 : comment line
line 2 : number of Wannier functions in model (ignored)
line 3 : how many integers to skip (here one integer)
line 4 : integers to skip ("-1" here... discarded)
line 5-10 : the tight-binding model, see below

Each tight-binding hopping element specified (lines 5-10) has the
simple format
R1 R2 R3  i j  t(R,i,j)  Imag
where
R1,R2,R3 are 3 integers specifying a lattice vector (crystal units)
i j label two Wannier orbitals
t(R,i,j) is real part of hopping element in eV
Imag is imaginary part of hopping element: should be zero, ignored anyways

In Dirac notation, this is specifying the value for
t(R,i,j) = <i0| H |jR>
where |jR> means Wannier orbital j in unit cell R.

