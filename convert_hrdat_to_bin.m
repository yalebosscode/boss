function convert_hrdat_to_bin(hrdatfilename,hrbinfilename,wann90version)
% function CONVERT_HRDAT_TO_BIN(hrdatfilename,hrbinfilename,vwann90)
% Converts the text (ASCII) <base>_hr.dat file output by Wannier90
% to a binary file.  It understands Wannier90 v1.1 and v1.2 formats
% Input:
%   hrdatfilename = string : <base>_hr.dat Wannier90 output filename)
%   hrdatfilename = string : output filename (preferably ending .bin)
%   wann90version = string: 1.1 or 1.2 curently are only allowed values

% Header of what is being done
fprintf('--- convert_hrdat_to_bin() ---\n')
fprintf('hrdatfilename=%s , hrbinfilename=%s , wann90version=%s\n',...
    hrdatfilename,hrbinfilename,wann90version)

% Open the text file and get the header line (discard it)
fprintf('Reading file %s\n',hrdatfilename)
[fid,errmsg] = fopen(hrdatfilename,'r');
if fid < 0
    disp(errmsg)
    stop
end
txt = fgets(fid);
fprintf('header: %s',txt)

% Check Wannier90 version and skip appropriate header block size
if wann90version == '1.1'
    ;
elseif wann90version == '1.2'
    tmp = textscan(fid,'%d',2);
    nskip = tmp{1}(2);
    fprintf('%d %d  ---  skipping %d integers\n',tmp{1}(1),nskip,nskip);
    textscan(fid,'%d',nskip);
else
    fprintf('\n\nERROR: Unknown Wannierf90 version (not 1.1 or 1.2)\n\n')
    stop
end

% Read the rest of the file which has the hoppings into a cell array
dcell = textscan(fid,'%d %d %d %d %d %f %f');
fclose(fid);

% How many hopings we have and change into array
nlines = size(dcell{4},1);
fprintf('Read %d hoppings\n',nlines);
d = zeros(nlines,7);
for j = 1:7
    d(:,j) = dcell{j};
end

% Write the data into the binary file
fprintf('Writing file %s\n',hrbinfilename)
[fid,errmsg] = fopen(hrbinfilename,'w');
if fid < 0
    disp(errmsg)
    stop
end
fwrite(fid,d,'double');
fclose(fid);

% Done!
fprintf('Conversion complete\n\n');

end

