
This directory contains an example a model system with manually created
tight-binding Wannier model for a 2D p-d system showing the simplest
possible type of orbital ordering as an illustrative example.

The system lies in a plane (e.g. xy plane) and has the structure of a
MO2 layer in a perovskite structure (M=metal atom) where the M atoms
are on the sites of a square lattice and the oxygens are at the mid-points
of all nearest-neighbot M-M pairs (hence, the oxygens form a square lattice
rotated by 45 degrees compared to the M lattice).  The M atoms have
two degenerate orbitals each and the O have one orbital each in this model:
this can represent the 2D t2g subsystem of (dxz,dyz) on the M sites and 
one pz on the O sites.  The largest hoppings are between the nearest
neighbor M-O pairs (magnitude 1.0) allowed by symmetry: so dxz couples to
pz along x only and dyz couples to pz along y only; this by itself would
create two separate 1D bands (a dxz band with dispersion along kx and
a dyz band with dispersion along ky) for the conduction bands.  To 
make the system 2D, weak pz-pz hoppings between nearest neighbor O 
are allowed (magnitude 0.3).

The unit cell has one MO2 unit in it and 4 orbitals.

The included setup_system.m file has U=Up=5, J=0.  It also includes
a non-zero "magnetic field" (symmetry breaking field) that makes 
orbital 2 of M1 lower in energy than orbital 1 of M1 and conversely
orbital 4 of M2 lower in energy that orbital 3 of M2 (the raising/lowering
of energies are +/-0.4 units of energy).  Running the main program
(after converting the 4orb_hr.dat file) shows that the B=0 solution
with equal orbital populations has a higher energy than the nonzero-B 
solution with unequal populations.  This example does not have any
magnetic ordering (i.e., zero net spin polarization on all sites) and
the subsidiary model has no spin resolution so as to make a clean
demonstration.

For larger U and especially non-zero J>0, this system shows a Mott
transition where the M-O hoppings become zeroed due to <O>=0 on the M
sites for one of the orbitals (the one that becomes half-filled).
