% DFT+Subsidiary-Particle Solver: Alexandru Georgescu, 2015-2017
%                                 Sohrab Ismail-Beigi, 2017-2022
%                                 Minjung Kim, 2017-2019

clear all
addpath('./functions')
format compact
format shortg

global verbose;
verbose = 0;

fprintf('\nDFT+Subsidiary-Particle Solver\n')
fprintf('Started on %s\n',datestr(now))
fprintf('Verbosity level verbose = %d\n',verbose)
fprintf('\n')

% Setup shop
fprintf('+++ Setting up physical system\n')
setup_system

% No symmetry breaking fields for the initial U=Up=J=0 run
fprintf('+++ Setting Bfield=0\n')
Bfieldsave = spinoninfo.Bfield;
spinoninfo.Bfield = spinoninfo.Bfield * 0;
    
% Run U=Up=J=0 (non interacting) problem once just to initialize
% and get bare quantities
Uinput = subinfo.U; Upinput = subinfo.Up; Jinput = subinfo.J;
subinfo.U = Uinput*0; subinfo.Up = Upinput*0; subinfo.J = Jinput*0;
fprintf('+++ Solving U=Up=J=0 & Bfield=0 problem to initialize\n');
solve_spinon_then_sub
% Save U=Up=J=0 chemical potential & particle count which are useful/needed
subinfo.dcount0 = dcount;
fprintf('dcount0(spin,corb) =\n')
disp([subinfo.dcount0])
fprintf('\n')
subinfo.U = Uinput; subinfo.Up = Upinput; subinfo.J = Jinput;
clear Uinput Upinput Jinput

% Calculate Etot with B=0 to compare the energy when B is not zero
if do_Bfield0_scf
    fprintf('+++ Calling main SCF loop to get Bfield=0 total energy\n')
    SCFloop
    EtotB0 = Etot;
    fprintf('   Etot(B=0) = %.8f\n\n',EtotB0);
end

% Add in Bfield "big B" symmetry breaking fields as set by user
fprintf('+++ Adding possible symmetry breaking fields\n')
%load Bfield
%Bfieldsave = Bfield; clear Bfield
spinoninfo.Bfield = Bfieldsave;
fprintf('Bfield = \n')
disp([spinoninfo.Bfield])
fprintf('\n')
if max(abs(size(dcount)-size(spinoninfo.Bfield))) > 0
    fprintf('**** Initial Bfield has wrong size!!!\n')
    fprintf('**** Bfield has same size as dcount.  Aborting.\n\n\n')
    stop
end

% If asked, read key state variable values from file
if load_state
    fprintf('+++ Reading state variables from file %s\n',load_state_file)
    load(load_state_file,'dcount','Oavg','C','h','tsub')
    subinfo.C = C;
    subinfo.h = h;
    subinfo.tsub = tsub;
    clear C h tsub
    fprintf('Read dcount(spin,corb) =\n')
    disp([dcount])
    fprintf('Read Oavg(spin,corb) =\n')
    disp([Oavg])
    fprintf('Read C, h, tsub\n')
    fprintf('\n')   
end

% The actual calculation: SCF loop for spinon+sub
fprintf('+++ Calling main SCF loop\n')
SCFloop

% If minimizing Etot over Bfield, go there...
if minimize_Etot_over_Bfield
    minimize_Etot
end

% If asked, save key state variable values to file at convergence
if save_state
    fprintf('+++ Saving state variables to file %s\n',save_state_file)
    C = subinfo.C;
    h = subinfo.h;
    tsub = subinfo.tsub;
    save(save_state_file,'dcount','Oavg','C','h','tsub');
    clear C h tsub
    fprintf('\n');
end


% Plot spinon bands
fprintf('+++ Plotting bands and PDOS\n')
plot_bands
plot_pdos
fprintf('\n')
