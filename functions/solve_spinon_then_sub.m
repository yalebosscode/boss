% This is a short script (not a function) that is just a space saver:
% runs spinon calculation on whatever we have handy for key variables,
% extracts subsidiary electron counts (dcount) and hoppings (tsub) then
% solves the subsidiary problem.  Spinon problem solved with "big B" Bfield

% Solve spinon problem with
[eklist,vklist,rho,mu,tRijscale,Eband] = solvespinon_fixedN(...,
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,Oavg,subinfo.site_of_corb);

% extract electron counts on correlated orbitals and hoppings
% onto d orbitals scaled by density matrix (i.e., the subsidiary hoppings)
dcount = getdcount(Rlist,orbs,corbs,rho);
subinfo.tsub = gettsub(orbs,corbs,porbs,tRij,rho);

% Solve subsidiary problem (over all sites)
[Oavg,Eint,subinfo] = sub_driver(subinfo,dcount,occtol);