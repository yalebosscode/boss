function kalist = create_spinon_kgrid(nk)
%function kalist = CREATE_SPINON_KGRID(nk)
% Create k-point grid for spinon calculation
% Input:
%   nk = [nkx nky nkz] = 3 integers specifying grid density along each axis
% Output:
%   kalist(:,1:3) = list of k-vectors are rows in crystal coordinates
%                   which are dimensionless going from -pi to pi

% Report.  Plus, if we fail, we see what dumb input we had
fprintf('--- create_spinon_kgrid:\n');
fprintf('Creating spinon kgrid of density %d x %d x %d\n',...
    nk(1),nk(2),nk(3));

% Sanity checks
for j=1:3
    if (nk(j)<1 || abs(round(nk(j))-nk(j))>1e-10)
        fprintf('nk(%d) = %g  is not a valid positive integer\n',j,nk(j))
        stop
    end
end

% now create the grid by first getting the lower and upper ranges
for j=1:3
    if mod(nk(j),2) == 0
        l(j) = -nk(j)/2;
        u(j) = nk(j)/2-1;
    else
        l(j) = -(nk(j)-1)/2;
        u(j) =  (nk(j)-1)/2;
    end
end
[jx,jy,jz]=meshgrid([l(1):u(1)],[l(2):u(2)],[l(3):u(3)]);
jx = jx/nk(1);
jy = jy/nk(2);
jz = jz/nk(3);
nktot = prod(nk);
kalist = 2*pi*...
    [reshape(jx,nktot,1) reshape(jy,nktot,1) reshape(jz,nktot,1)];

end %function

