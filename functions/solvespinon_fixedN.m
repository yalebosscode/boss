function [eklist,vklist,rho,mu,tRijscale,Eband] = solvespinon_fixedN(...
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,Oavg,site_of_corb)
%function [eklist,vklist,rho,mu,tRijscale,Eband] = SOLVESPINON_FIXEDN(...
%    spinoninfo,orbs,corbs,porbs,tRij,Rlist,Oavg,site_of_corb)
% Solves the spinon problem for a given number of spinons spinoninfo.Ne via
% diagonalization and adjustment of chemical potential mu at temparature
% spinoninfo.kTspinon .  Also computes band energy = sum over eigenvalues
% with occupations but without Bfield contributions (hopping+onsite).
% Input:
%   spinoninfo = contains spinon information (k vectors, # electrons, ...)
%   norbs =  total number of orbitals
%   orbs = list of orbitals (correlated followed by uncorrelated)
%   corbs = correlated orbitals
%   porbs = uncorrelated orbitals
%   tRij(iR,1:norbs,1:norbs,spin) = bare TB Hamiltonian in eV where iR
%              labels R vectors (referenced to home cell R=000) (eV)
%   Rlist(iR,1:3) = list of R vectors 
%   Oavg(spin,1:length(corbs)) = <O> for the correlated orbitals
%   site_of_corb(1:length(corbs)) = correlate orbital to site mapping
% Output:
%   eklist(ik,:,spin) = eigevnalues (eV)
%   vklist(ik,:,:,spin) = eigenvectors in columns
%   rho(iR,:,:,spin) = real-valued density matrix in real space
%   mu = chemical potential (eV)
%   tRijscale = appropriately scaled tight-binding spinon model (with <O>
%               for p-d hoppings and -B+Dpd for on site values)
%   Eband = spinon band energy excluding B term but including Dpd

global verbose

% get params from spinoninfo struct
Ntarget = spinoninfo.Ne;
Ntol = spinoninfo.Ntol;
kalist = spinoninfo.kalist;
B = spinoninfo.Bfield;
Dpd = spinoninfo.Dpd;
kTspinon = spinoninfo.kTspinon;

if verbose
    fprintf('--- solvespinon_fixedN: nk=%d  Ntarget=%d  Ntol=%g\n',...
        size(kalist,1),Ntarget,Ntol)
end

% create spinon Hamiltonian in TB representation
tRijscale = buildHspinon(orbs,corbs,porbs,tRij,Rlist,Oavg,site_of_corb,B,Dpd);

% diagonalize H spinon over k points;
norbs = length(orbs);
[eklist,vklist] = diagHspinon(kalist,norbs,Rlist,tRijscale);

% Compute number of spinons
mu = findmu(eklist,Ntarget,kTspinon,Ntol);

% compute density matrix
rho = calcrhospinon(eklist,vklist,mu,kalist,Rlist,kTspinon);

% get dcount from rho
dcount = getdcount(Rlist,orbs,corbs,rho);

% Band energy: the added term cancels same contribution inside tRijscale
% so that the final Eband has no "B" contribution
Eband = sum(sum(sum(sum(rho.*tRijscale))))+sum(sum(sum(B.*dcount)));

end %function
