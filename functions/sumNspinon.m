function Nspinon = sumNspinon(eklist,mu,kTspinon)
% function Nspinon = SUMNSPINON(eklist,mu,kTspinon)
% Compute number of spinons from spinon eigenvalues and chemical potential
% Input:
%    eklist(ik,:,spin) = list of spinon energies spin up (eV)
%    mu = chemical potential (eV)
%    kTspinon = temperature for spinon FD distribution (eV)

global verbose
if verbose > 1
    fprintf('--- sumNspinon: N(Enk)=%d  mu=%g  kTspinon=%g\n',...
        numel(eklist),mu,kTspinon);
end

% use FD fillings to compute total number of spinons
f = 1./(1+exp((eklist-mu)/kTspinon));
nktot = size(eklist,1);
Nspinon = sum(sum(sum(f)))/nktot;

end % function
