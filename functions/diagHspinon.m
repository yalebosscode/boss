function [eklist,vklist] = diagHspinon(kalist,norbs,Rlist,tRijscale)
% function [eklist,vklist] = DIAGHSPINON(kalist,norbs,Rlist,tRijscale)
% Given the spinon Hamiltonian given by the hopping data tRijscale
% it builds and diagonalizes spinon Hamiltonians over all k-points
% and stores in the output arrays.  spin=1:2 below
% Input:
%   kalist(:,1:3) = list of k*a k-vectors
%   norbs = total number of orbitals in tight-binding spinon system
%   Rlist(iR,1:3) = list of R vectors 
%   tRijscale(iR,1:norbs,1:norbs,spin) = bare TB Hamiltonian in eV where
%         iR labels R vectors (referenced to home cell 000)
% Output: 
%   eklist(ik,:,spin) = eigevnalues (eV)
%   vklist(ik,:,:,spin) = eigenvectors in columns

global verbose;
nktot = size(kalist,1);
if verbose
    fprintf('--- diagHspinon: diagonalizing over %d k points\n',nktot)
end
% Diagonalize the spinon Hamiltonian over all
% kpoints and store data in arrays
eklist = zeros(nktot,norbs,2);
vklist = zeros(nktot,norbs,norbs,2);
for j=1:nktot
  ka = kalist(j,:)';
  [vkup,ekup] = diagH_kspinon(ka,Rlist,tRijscale(:,:,:,1));
  [vkdn,ekdn] = diagH_kspinon(ka,Rlist,tRijscale(:,:,:,2));
  eklist(j,:,1)   = ekup;
  eklist(j,:,2)   = ekdn;
  vklist(j,:,:,1) = vkup;
  vklist(j,:,:,2) = vkdn;
end

end % function
