function tRijscale = buildHspinon(orbs,corbs,porbs,tRij,Rlist,Oavg,...
    site_of_corb,B,Dpd)
%function tRijscale = BUILDHSPINON(orbs,corbs,porbs,tRij,Rlist,Oavg,site_of_corb,B,Dpd)
% Creates the spinon Hamlitonian and puts it into tRijscale which 
% is a real-space tight-binding representation.  This means scaling the
% p-d hoppings by <O>, adding in the symmetry breaking fields B on diagonal
% and a possible p-d splitting correction.  spin=1:2 below
% Input:
%   orbs(1:norbs) = list of orbitals where norbs = size(orbs) below
%   corbs(:) = list of correlated orbitals
%   porbs(:) = list of uncorrelated orbitals
%   tRij(iR,1:norbs,1:norbs,spin) = bare TB Hamiltonian in eV where iR
%              labels R vectors (referenced to home cell R=000)
%   Rlist(:,1:3) = list of R vectors corresponding to reach row of tRij
%   Oavg(spin,1:length(corbs)) = <O> for the correlated orbitals
%   site_of_corb(1:length(corbs)) = correlated orbital to site mapping
%   B(spin,1:size(corbs)) = "Big B" fields controlling occupancies
%   Dpd = possible p-d splitting increase (eV)
% Output:
%   tRijscale(iR,1:norbs,1:norbs,spin) = same layout as tRij but
%           with scaled p-d hoppings and -B+Dpd added on diagonals

global verbose
if verbose
    fprintf('--- buildHspinon: Dpd = %g\n',Dpd);
    fprintf('Oavg = \n');
    disp ([Oavg])
end

% we need to scale the d-p hoppings to/from the d sites
% by the average of the subsidiary-boson operator expectation value <O>=sqrt(Z)
% First copy them over from "bare" hoppings (tRij) then scale
tRijscale = tRij;
for po = porbs
    for do = corbs
        ip = find( orbs==po );
        id = find( orbs==do );
        isubdo = find( corbs==do );
        for is = 1:2
            tRijscale(:,ip,id,is) = tRijscale(:,ip,id,is)*Oavg(is,isubdo);
            tRijscale(:,id,ip,is) = tRijscale(:,id,ip,is)*Oavg(is,isubdo);
        end
    end
end
clear po do ip id dsite is

% Need position in "hopping" list that is R=000 (home cell) and complement 
iR0 =    find( sum(Rlist.^2,2) == 0 );
iRnot0 = find( sum(Rlist.^2,2) > 0  );
nR = size(Rlist,1);

% Now loop over d-d hoppings that are not on the same site and
% scale by two <O> products.  Otherwise we would get bare hopping between
% correlated sites despite killing p-d hoppings for large U which makes
% for weird approach to Mott transition (or lack thereof)
for di = corbs
    for dj = corbs
        odi = find( orbs==di );
        odj = find( orbs==dj );
        isubdi = find( corbs==di );
        isubdj = find( corbs==dj );
        isite = site_of_corb(isubdi);
        jsite = site_of_corb(isubdj);
        if isite == jsite
            list = iRnot0;
        else
            list = [1:nR]';
        end
        for is = 1:2
            tRdidjs = tRijscale(list,odi,odj,is);
            OisOjs = Oavg(is,isubdi)*Oavg(is,isubdj);
            tRijscale(list,odi,odj,is) = tRdidjs*OisOjs;
        end
    end
end
clear di dj odi odj isubdi isubdj isite jsite list is OisOjs  tRdidjs

% Add onsite corrections:  loop over correlated orbitals and add
% -B term to "hopping" (digonal is on site energy) and possible
% p-d splitting adjustment
for j=1:length(corbs)
    do = corbs(j);
    id = find( orbs==do );
    for is = 1:2
        tRijscale(iR0,id,id,is) = tRijscale(iR0,id,id,is) - B(is,j) + Dpd;
    end
end

end %function
