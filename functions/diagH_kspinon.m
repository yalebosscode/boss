function [vk,ek] = diagH_kspinon(ka,Rlist,tRij)
% function [vk,ek] = DIAGH_KSPINON(ka,Rlist,tRij)
%  Diagonalize the spinon Hamiltonian for a k point.
%  Input: 
%    ka(1:3) = column vector in 2*pi units specifing k
%    tRij(1:nR,1:norbs,1:norb) = hopping element list
%    Rlist(1:nR,1:3) = R vectors for hopping elements
%  Output:
%    vk = matrix of eigenvectors (vectors in columns)
%    ek = row vectors of eigenvalues

% The convention on the Fourier transform may look funny (positive
% sign in exponential instead of usual exp(-ikR) when going from R
% to k) but it is correct due to definition of tRij = <0i|H|Rj> since
% Hk(i,j) = sum_R <0i|H|Rj>*exp(ik.R) is folding in Bloch phases.
i = sqrt(-1);
kaR = Rlist(:,1:3)*ka;
HkRij = bsxfun(@times,exp(i*kaR),tRij);
Hk = squeeze(sum(HkRij,1));
[vk,ek] = eig( (Hk+Hk')/2 );
ek = diag(ek)';
