function [Oavg,Eint,subinfo] = sub_driver(subinfo,dcount,occtol)
% function [Oavg,Eint,subinfo] = SUB_DRIVER(subinfo,dcount,occtol)
% This routine loops over all the correlated sites and solves the subsidiary
% problem on each one by (a) first running U=J=0 and adjusting C and h
% until occupancies match gives dcount ones and <O>=1, and then (b) 
% solving the interacting site and adjusting h until occupancies match.
% Input:
%   subinfo = data structure containing details of subsidiary problem
%   dcount(spin,ncorbs) = incoming occupancies to match (from spinon)
%   occtol = tolernace on occupancy matching
% Output:
%   Oavg(spin,ncorbs) = final <O> values 
%   Eint = sum over all subsidiary interaction energies over all sites
%   subinfo = C and h inside subinfo are set to updated values

global verbose;

% get key data from subinfo (shorter names)
tsub = subinfo.tsub;
h = subinfo.h;

% initialize before looping over sites
Oavg = zeros(size(dcount));
counts = zeros(subinfo.nsites,subinfo.nsubspersite);
counts0 = zeros(subinfo.nsites,subinfo.nsubspersite);
tsum = zeros(subinfo.nsites,subinfo.nsubspersite);
Einti = zeros(subinfo.nsites,1);
hstart = zeros(subinfo.nsites,subinfo.nsubspersite);
for isite = 1:subinfo.nsites
    for isub = 1:subinfo.nsubspersite
        % table saying which spin+orbital are for a given sub mode
        table = subinfo.statetable{isite}{isub};
        % sum up occupancies and hoppings of spin+orbital set for this
        % sub mode
        for j = 1:size(table,1)
            icorb = table(j,1);
            ispin = table(j,2);
            counts(isite,isub)  = counts(isite,isub)  + ...
                dcount(ispin,icorb);
            counts0(isite,isub) = counts0(isite,isub) + ...
                subinfo.dcount0(ispin,icorb);
            tsum(isite,isub) = tsum(isite,isub) + tsub(ispin,icorb);
            hstart(isite,isub) = h(ispin,icorb);
        end
    end
    if verbose
        fprintf('--- sub_driver() working on isite=%d\n',isite);
    end
    % extract this site's key info
    countsi = counts(isite,:);
    counts0i = counts0(isite,:);
    tsumi = tsum(isite,:);

    % Search over C and h at U=Up=J=0 to match occupancies and <O>=1
    subinfo = Csearch(countsi,tsumi,occtol,subinfo,isite);
    [Oavgi,hi,Einti(isite),subinfo] = hsearch(countsi,counts0i,...
        tsumi,hstart(isite,:),occtol,...
        subinfo,isite);

    % Now put the found values of hi and Oavg into the output structures
    for isub = 1:subinfo.nsubspersite
        table = subinfo.statetable{isite}{isub};
        for j = 1:size(table,1);
            icorb = table(j,1);
            ispin = table(j,2);
            h(ispin,icorb) = hi(isub);
            Oavg(ispin,icorb) = Oavgi(isub);
        end
    end
       
end % over sites (isite)

% total interaction energy
Eint = sum(Einti);

% copy back the h we just found into subinfo to be returned
subinfo.h = h;

% save some other values into the output structure as well
subinfo.Occsref = counts0;
subinfo.Occssub = counts;
subinfo.tsum = tsum;

end
