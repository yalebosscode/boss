function rho = calcrhospinon(eklist,vklist,mu,kalist,Rlist,kTspinon)
% function rho = CALCRHOSPINON(eklist,vklist,mu,kTspinon)
% Given eigenvalues and eigenvectors and chemical potential mu 
% for the spinon system, compute the density matrix of spinons with
% proper FD thermal distribution.  Convention is rho(d,p) = <f_d^dag f_p>
% Input:
%   eklist(ik,:,spin) = eigevnalues (eV)
%   vklist(ik,:,:,spin) = eigenvectors in columns
%   mu = specified chemical potential (eV)
%   kalist(ik,1:3) = k vectors in dimensionless units of 2*pi ("k*a")
%   Rlist(iR,1:3) = list of R vectors 
%   kTspinon = spinon temperature (eV) for FD distribution
% Output:
%   rho(iR,j,k,spin) = real-valued density matrix <f_{j0}^dag f_{kR}>

global verbose;
if verbose
    fprintf('--- calcrhospinon: mu=%g  kTspinon=%g\n',mu,kTspinon);
end

% Fermi-Dirac fillings
fup = 1./(1+exp((eklist(:,:,1)-mu)/kTspinon));
fdn = 1./(1+exp((eklist(:,:,2)-mu)/kTspinon));
% compute rho contribution at each k
nktot = size(eklist,1);
for k=1:nktot
    ukup = squeeze(vklist(k,:,:,1));
    ukdn = squeeze(vklist(k,:,:,2));
    rhokup(k,:,:) = ukup*diag(fup(k,:))*ukup';
    rhokdn(k,:,:) = ukdn*diag(fdn(k,:))*ukdn';
end
clear k ukup ukdn f

% the above rho(x,y) = sum_n occup(n)*psi_n(x)*conj(psi_n(y)) is
% the usual textbook formula for a density matrix.  However, we
% want the slightly different convention rho(d,p) = <d^dag p>
% which is the conjugate of textbook.  This is because <d^dag p>, e.g., 
% is defined the multiply exp(i*theta) in a rotor problem because
% we defined the rotor phase to be positive for addded particles (just
% to make an example with a simple subsidiary rotor case).
rhokup = conj(rhokup) ;
rhokdn = conj(rhokdn);

% sum over k vectors with phases to get density matrix entries
% since we are looking for rho(R,j,k) = <f_{j0)^dag f_{kR}> the
% phase factor turns out to be exp(+ikR) when worked out.
i = sqrt(-1);
nR = size(Rlist,1);
norb = size(rhokup,2);
rhoup = zeros(nR,norb,norb);
rhodn = zeros(nR,norb,norb);
for iR=1:nR
    R = Rlist(iR,:);
    rhoup(iR,:,:) = sum(bsxfun(@times,exp(i*kalist*R'),rhokup),1);
    rhodn(iR,:,:) = sum(bsxfun(@times,exp(i*kalist*R'),rhokdn),1);
end
rhoup = rhoup / nktot;
rhodn = rhodn / nktot;
clear rhokup rhokdn j

% we assume rho is real so toss imaginary part
rho(:,:,:,1) = real(rhoup);
rho(:,:,:,2) = real(rhodn);

end %function
