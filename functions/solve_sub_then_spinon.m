% This is a short script (not a function) that is just a space saver:
% runs subsidiary calculation on whatever we have handy for key variables
% to match whatever dcount we have on hand.  So the main output here
% is the <O>.  Then we call the spinon with these <O> and get the
% updated dcount (and subsidiary hoppings).  The spinon has the "big B"
% local fields from Bfield acting on it as a potential

% Solve subsidiary problem (over all sites)
[Oavg,Eint,subinfo] = sub_driver(subinfo,dcount,occtol);

% Solve spinon problem
[eklist,vklist,rho,mu,tRijscale,Eband] = solvespinon_fixedN(...
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,Oavg,subinfo.site_of_corb);

% extract electron counts on correlated orbitals and hoppings
% onto d orbitals scaled by density matrix (i.e., the subsidiary hoppings)
dcount = getdcount(Rlist,orbs,corbs,rho);
subinfo.tsub = gettsub(orbs,corbs,porbs,tRij,rho);
