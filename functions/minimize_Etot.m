% This sub program (script) minimizes the total energy Etot
% over Bfield entries using the simplest steepest descent algorithm
% (i.e. move down the gradient).  Gradients are computed by finite
% differences so it can take a while.  It creates its own log file to
% keep track of progress without the clutter of the main program.

% open a log file just for key minimization output and logging
minlogfid = fopen(miniminfo.logfile,'w');

% print out settings
fprintf(minlogfid,'+++ Minimization settings +++\n');
fprintf(minlogfid,'Etot tolerance = %.1e eV\n',miniminfo.Etottol);
fprintf(minlogfid,'Initial gamma = %g\n',miniminfo.gamma);
fprintf(minlogfid,'dBfield = %g eV\n\n',miniminfo.dBfield);

% Record information on which subsidiary-boson model we use for this run
fprintf(minlogfid,'+++ Subsidiary Information +++\n');
fprintf(minlogfid,'nsites = %d\nnsubs/site = %d\nncorbs/sub = %d\n',...
    subinfo.nsites,subinfo.nsubspersite,subinfo.ncorbspersub);
fprintf(minlogfid,'spinresolved = %d\n',subinfo.spinresolved);
fprintf(minlogfid,'orbresolved = %d\n',subinfo.orbresolved);
fprintf(minlogfid,'Allowed occupancies per sub = ');
fprintf(minlogfid,'%d ',subinfo.allowedOccs);
fprintf(minlogfid,'\nU = ');
fprintf(minlogfid,'%g ',subinfo.U);
fprintf(minlogfid,'\nUp = ');
fprintf(minlogfid,'%g ',subinfo.Up);
fprintf(minlogfid,'\nJ = ');
fprintf(minlogfid,'%g ',subinfo.J);
fprintf(minlogfid,'\n\n');

% report B=0 value if calculation was done
if exist('EtotB0')
    fprintf(minlogfid,'+++ Energy without symmetry breaking field +++\n'); 
    fprintf(minlogfid,'Etot(B=0)=%f\n\n\n',EtotB0);                   
end

% Main program just did the calculation with incoming Bfield, so save
% that starting energy
E0 = Etot;
minlogmatrix(minlogfid,spinoninfo.Bfield,'Initial Bfield =');
minlogmatrix(minlogfid,dcount,'Initial dcount =');
minlogmatrix(minlogfid,Oavg,'Initial Oavg =');
fprintf(minlogfid,'Initial Etot = %.8f eV\n\n',E0);

% Initialize the minimization
iter = 0;
Enew = E0;

% Start minimizing
dEtot = 1e20; % ludicrous huge number to initialize
gamma = miniminfo.gamma;
just_reset_gamma = 0;
while dEtot > miniminfo.Etottol || just_reset_gamma
    
    % report iteration number
    iter = iter + 1;
    timestamp = clock;
    fprintf(minlogfid,'+++ ITERATION %d   time=%d:%d:%.2f\n\n',...
        iter,timestamp(4:6));
    fprintf('+++ MINIMIZATION ITERATION %d  time=%d:%d:%.2f\n\n',...
        iter,timestamp(4:6));

    % Compute B derivs of Etot (grad) via finite differencing
    E0 = Enew;
    if ~just_reset_gamma
        nB = prod(size(spinoninfo.Bfield));
        Bfield0 = spinoninfo.Bfield;
        for j = 1:nB
            fprintf('+++ Bfield derivative %d/%d iteration %d\n',j,nB,iter);
            Bvec = reshape(Bfield0,nB,1);
            Bvec(j) = Bvec(j) + miniminfo.dBfield;
            spinoninfo.Bfield = reshape(Bvec,size(spinoninfo.Bfield));
            SCFloop
            dEtotdB(j) = (Etot-E0)/miniminfo.dBfield;
        end
        spinoninfo.Bfield = Bfield0;
        grad = reshape(dEtotdB,size(spinoninfo.Bfield));
        clear Bfield0 j Bvec dEtotdB nB j
    end
    
    % Move along gradient
    spinoninfo.Bfield = spinoninfo.Bfield - gamma*grad;
    
    % Get new shifted energy
    fprintf('+++ Getting shifted energy iteration %d\n',iter);
    SCFloop
    Enew = Etot;
    fprintf(minlogfid,...
        'E0 = %.8f  Enew = %.8f  dE = %.1e  |gradient| = %.1e\n',...
        E0,Enew,Enew-E0,norm(grad));

    % If energy went up, report, reverse step, reduce gamma and try again
    if Enew > E0
        spinoninfo.Bfield = spinoninfo.Bfield + gamma*grad;
        Enew = E0;
        gamma = gamma / miniminfo.gammazapfac;      
        fprintf(minlogfid,'Energy went up! Reducing gamma to %g ',gamma);
        fprintf(minlogfid,'and undoing move\n\n');
        % sometimes, gamma gets very small.
        if gamma < miniminfo.gammamin
            fprintf(minlogfid,'+++ WARNING: gamma became too small... Stopping iteration +++\n')
            fprintf(minlogfid,'Reporting final energy Etot(final)=%f\n',Enew)
            minlogmatrix(minlogfid,spinoninfo.Bfield,'Bfield =');
            minlogmatrix(minlogfid,dcount,'dcount =');
            minlogmatrix(minlogfid,Oavg,'Oavg =');         
            break
        end
        just_reset_gamma = 1;
        continue;
    % Energy went down yohoo, report and increase gamma
    else
        gamma = gamma * miniminfo.gammagrowfac;  
        fprintf(minlogfid,'gradient = ');
        fprintf(minlogfid,'%g ',grad);
        fprintf(minlogfid,'\nEnergy went down: new gamma = %g\n\n',gamma);
        just_reset_gamma = 0;
    end
    
    % value to test convergence
    dEtot = abs( Enew - E0 );
    
    minlogmatrix(minlogfid,spinoninfo.Bfield,'Bfield =');
    minlogmatrix(minlogfid,dcount,'dcount =');
    minlogmatrix(minlogfid,Oavg,'Oavg =');
    fprintf(minlogfid,'\nEtot(%d) = %.10f\n',iter,Enew);
    fprintf(minlogfid, '\n\n');
    
end

% final reporting then close log file
timestamp = clock;
fprintf(minlogfid,'+++ Converged!  time=%d:%d:%.2f\n',timestamp(4:6));
if exist('EtotB0') && EtotB0 < Enew
    fprintf(minlogfid,'\n+++ WARNING: Etot(B=0) is lower than minimized energy\n');
    fprintf('\n+++ WARNING: Etot(B=0) is lower than minimized energy\n');
end

fclose(minlogfid);

% Local helper routine to print 2D matrices to log file 
function minlogmatrix(minlogfid,data,name)
    nr = size(data,1);
    nc = size(data,2);
    fprintf(minlogfid,name);
    fprintf(minlogfid,'\n');
    for r = 1:nr
        for c = 1:nc
            fprintf(minlogfid,' %g ',data(r,c));
        end
        fprintf(minlogfid,'\n');
    end
end
