function subinfo = Csearch(Occstomatch,t,occtol,subinfo,isite)
% function subinfo = CSEARCH(Occstomatch,t,occtol,subinfo,isite)
% Finds the C and h values for the non-interacting subsidiary case for
% a given site so that occupancies match and <O>=1  for each of the n
% subsidiary modes separately.  This is a single site connected to a bath, and
% the number n = # of subsidiaries on a site
% Input:
%   Occstomatch(1:n) = subsidiary occpancies to match
%   t(1:n) = hoppings onto site from bath (includes minus sign)
%   occtol = tolerance on maximum occupancy matching error
%   subinfo = C, h, needed subsidiary tables and indices are in this class
%   isite = site index of the subsidiary problem (for looking up in subinfo)
% Output:
%   subinfo = C is updated inside this on return to values found

% The procedure followed by this routine actually does not need the
% the numerical value of hopping t(l) parameter to do its main work for
% bosonic mode l. Matching occupancies and setting <O>=1 are both 
% dimensionless procedures: if some values of C(l) and h(l) work for
% some input t(l), then C(l) and s*h(l) will work for s*t(l) where s is
% a constant scale.  So the routine actually solves the problem for
% t(l)=-1 and at the very end rescales h by t(l).  The reason we do this
% is because sometimes for extreme occupancies, the t(l) become positive,
% which is not the usual physical situation, so then the lowest 
% eigenvalue's eigenvector is no longer the one we want (it will be 
% quite oscillatory instead of the most constant).  The solution is to
% solve under physical conditions where t(l) is negative and then scale
% the final output.

global verbose;

% get local copies of key variables
n = subinfo.nsubspersite;
allowedOccs = subinfo.allowedOccs;
Oavgtol = subinfo.Oavgtol;
dh = subinfo.dh;
dC = subinfo.dC;

if verbose
    fprintf('--- Csearch: n=%d occtol=%g Oavgtol=%g dh=%g dC=%g\n',...
        n,occtol,Oavgtol,dh,dC)
    fprintf('allowedOccs = '); disp ([allowedOccs])
    fprintf('Occstomatch = '); disp ([Occstomatch])
    fprintf('t = '); disp([t])
    fprintf('Starting C = '); disp([subinfo.C(isite,:)])
end

% If special case of spin+orbital resolution, then we have
% a formula to compute C and can be done quickly.  This is 
% equation (2.25) from Hassan and de' Medici, PRB 81 035106 (2010)
% which says c = sqrt(1/(n(1-n)) - 1  for occupancy n of sub mode
if subinfo.spinresolved && subinfo.orbresolved
    for l = 1:n
        nl = Occstomatch(l);
        subinfo.C(isite,l) = sqrt(1/(nl*(1-nl)))-1;
    end
    if verbose
        fprintf('spin & orbital resolution: used formula and got\n');
        fprintf('C = '); disp([subinfo.C(isite,:)])
    end
    return
end

% Rescaling needed for most detailed subsidiary description (this
% is about comparing to the literature as "t" is defined in a specific
% way in hopping Hamiltonians whereas with a spin+orbital description
% with only two occupancies for a sub mode, we get periodic wrapping
% that effectively doubles t.
if (length(allowedOccs)==2)
    %fprintf('*** Special case!  t->t/2 due to length(allowedOccs)==2\n');
    t = t/2;
end

% non-interacting case so these are zero or irrelevant
U = 0;
Up = 0;
J = 0;

% starting guesses and initialization
h = zeros(1,n);
Oavg = ones(1,n)*2;
occs = zeros(1,n);
OavgCsh = zeros(1,n);
occsCsh = zeros(1,n);
Oavghsh = zeros(1,n);
occshsh = zeros(1,n);

% Our sub subsidiary solver uses temperature to compute  average
% occupancies and <O>.  Since we are solving each subsidiary mode l
% with t=-1 we can set a working temperature for solution to be 0.01
% (dimensionless) to isolate the ground state.  We save the actual
% value to avoid overwriting permanently.
kTsub = subinfo.kTsub;
kTsolve = 0.01;
subinfo.kTsub = kTsolve;

% Loop over each subsidiary mode separately: when there is no interaction,
% we have sum over hoppings per subsidiary mode and sum over Lagrange h
% term for each subsidiary, so the subsidiary Hamiltonian becomes 
% completely separable so we solve for C and h for each mode l separately
for l = 1:n
    if verbose > 1
        fprintf('* Csearch working on l=%d out of n=%d\n',l,n)
    end
    count = 0;
    alpha = 0.5;
    
    % We use Newton's algorithm to find the pair (C(l),h(l)).  Derivatives
    % are computed by finite differencing by dh and dC (input params).
    % The step suggested by Newton's algorithm is scaled by 0<alpha<1 for
    % convervativeness and safety; if progress is made to the solution,
    % alpha iterates to 1 for full Newton step and its quadratic 
    % convergence; if we are not making progress, alpha is divided by 3
    % to simulate a "reset".
    while abs(Occstomatch(l)-occs(l)) > occtol | abs(Oavg(l)-1) > Oavgtol
        % record stuff for this iteration
        count = count+1;
        doccs = abs(Occstomatch(l)-occs(l));
        dOavg = abs(Oavg(l)-1);
        % compute Oavg and occupancies and their derivs 
        % versus h and C numerically
        [Oavg(l),occs(l),~] = HamSub(U,Up,J,0,...
            -1,h(l),      subinfo.C(isite,l),1,subinfo,isite);
        [OavgCsh(l),occsCsh(l),~] = HamSub(U,Up,J,0,...
            -1,h(l),   subinfo.C(isite,l)+dC,1,subinfo,isite);
        [Oavghsh(l),occshsh(l),~] = HamSub(U,Up,J,0,...
            -1,h(l)+dh, subinfo.C(isite,l),1,subinfo,isite);
        % numerical 2x2 derivative matrix of occs and Oavg versus C and h
        Dmatrix = [ (occsCsh(l)-occs(l))/dC  (occshsh(l)-occs(l))/dh
            (OavgCsh(l)-Oavg(l))/dC  (Oavghsh(l)-Oavg(l))/dh ];
        if verbose > 1
            fprintf('|doccs|=%g  |Oavg-1|=%g  ',...
                abs(Occstomatch(l)-occs(l)),abs(Oavg(l)-1));
            fprintf('occ=%g  Oavg=%g  C=%g   h=%g  alpha=%g\n',...
                occs(l),Oavg(l),subinfo.C(isite,l),h(l),alpha)
            %disp 'Dmatrix'
            %disp([Dmatrix])
        end
        % abort if too many steps
        if count > 10000
            disp 'Too many Newton steps (>10,000) to find C,h for U=J=0'
            fprintf('Was working on sub mode %d\n',l);
            [Occstomatch(l) occs(l) Oavg(l) subinfo.C(isite,l) h(l) alpha]
            stop
        end
        % scaled Newton step: if we do well, send alpha to 1; if not,
        % whack it down by a factor of 3 to "reset"
        if count > 1
            if doccs < doccsold && dOavg < dOavgold
                alpha = alpha*(2-alpha);
            else
                alpha = alpha/3;
            end
        end
        sh = -alpha*inv(Dmatrix)*[occs(l)-Occstomatch(l) Oavg(l)-1]';
        subinfo.C(isite,l) = subinfo.C(isite,l) + sh(1);
        h(l) = h(l) + sh(2);
        
        % record for next iter
        doccsold = doccs;
        dOavgold = dOavg;
    end %while
end % for l over subsidiary modes

% scale the h values back to scale set by t
h = h.*(t/(-1));

if verbose
    fprintf('Csearch concluded.  We have:\n');
    fprintf('C = '); disp([subinfo.C(isite,:)])
    fprintf('h = '); disp([h])
    fprintf('<O> = '); disp([Oavg])
    fprintf('occs = '); disp([occs])
end

% reset the subsidiary temperature to actual value
subinfo.kTsub = kTsub;

end % function
