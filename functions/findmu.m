function mu = findmu(eklist,Ntarget,kTspinon,Ntol)
% function mu = FINDMU(eklist,Ntarget,kTspinon,Ntol)
% Find chemical potential mu (eV) so that the number of spinons
% is within Ntol of Ntarget given the spinon eigenvalues.
% Input:
%    eklist(ik,:,spin) = list of spinon energies (eV)
%    Ntarget = target spinon count ("number of electrons")
%    kTspinon = temperature for spinon FD distribution (eV)
%    Ntol = tolerance for total electon count

global verbose
if verbose
    fprintf('--- findmu: Ntarget=%d  kTspinon=%g  Ntol=%g\n',...
        Ntarget,kTspinon,Ntol)
end

% Use classic bisection to find chemical potential mu to tolerance
mulow = min(eklist(:)) - max(1,kTspinon);
muhigh = max(eklist(:)) + max(1,kTspinon);
mu = (mulow + muhigh) / 2;
niter = 1;
while 1
    Nspinon = sumNspinon(eklist,mu,kTspinon);
    if abs(Nspinon-Ntarget) < Ntol
        break;
    end
    if Nspinon > Ntarget 
        muhigh = mu;
    else
        mulow = mu;
    end
    mu = (mulow+muhigh)/2;   
    niter = niter + 1;
    if niter > 100
        fprintf('>100 mu-finding bisection iterations in findmu()\n');
        fprintf('Is the target # of electrons = %g correct?\n',Ntarget);
        stop
    end
end

if verbose
    fprintf('Final mu = %g   |dN| = %g\n',mu,abs(Nspinon-Ntarget))
end

end %function