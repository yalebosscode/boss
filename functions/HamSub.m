function [Oavg,occs,Eint,energies,V,prob,configs,pconfigs,Nopeig] = ...
    HamSub(U,Up,J,meanOccs,t,h,C,n,subinfo,isite)
% function [Oavg,occs,Eint,energies,configs,pconfigs] = ...
%   HAMSUB(U,Up,J,meanOccs,t,h,n,subinfo,isite)
% Solves subsidiary boson problem for a single site with n bosonic modes.
% Input:
%   U, Up, J = interaction parameters U, Uprime, J values in eV
%   meanOccs(1:n) = electron occpancy where each subsidiary at "half filling"
%              by which we mean the interaction term is zero.  This is
%              the occupation the subsidiary is trying to actually match.  For
%              example, for a U term we have Hint=(U/2)*(N-<N>)^2 where
%              <N>=sum(meanoccs) is the total mean electron number on site
%              and various subdivisions of meanOccs play the same role
%              for the Up and J interaction terms
%   t(1:n) = hopping energy (eV) for each boson; includes minus sign in it
%   h(1:n) = Lagrange multiplier energy for each boson
%   C(1:n) = "C" numbers in corner of O/Odag operators that ensure
%            correct occupations and <O>=1 in non-interacting limit
%   n = # of subsidiary modes being solved for, usually equals 
%       subinfo.nsubsspersite but does not have to.  If equal, then
%       routine can can add Up and J dependent energies
%   subinfo = needed subsidiary tables and indices are in this class
%   isite = site index of the subsidiary problem (for looking up in subinfo)
% Output:
%   Oavg(1:n) = <O> for each bosonic mode (thermal)
%   occs(1:n) = mean occupancy of boson mode (thermal)
%   Eint = mean interaction energy (thermal)
%   energies(:) = bosonic Hamiltonian's eigenvalues
%   V(:,:) = bosonic Hamiltonian's eigenvectors in columns
%   prob = Boltzmann probabilities of bosonic eigenstates (thermal)
%   configs(:,1:n) = the bosonic configurations available: each row is a 
%                    config while the columns label the bosons and
%                    each entry is the appropriate occupancy
%   pconfigs(:) = probabilities of the configurations (thermal)
%   Nopeig{1:n}(:,:) = Number operators of sub modes in eigenbasis

% The size of the occupancy list we use which is the size of
% the bosonic Hilbert space for each boson (separately)
allowedOccs = subinfo.allowedOccs;
nmax = length(allowedOccs);

% We have n subsidiary bosons on the site of interest.  So our bosonic
% Hilbert space will be nmax^n.  Here we will set up the basic
% operators in the full bosonic Hilbert space: Odag (raising) operators
% which have C in the corner and the diagonal number operators.
% l will index over the subsidiary bosons on the site (1:n).
% tOdag(:,:,l) is t*Odag (hopping in only)
% Nop(:,:,l) is the number operator
% hNop(:,:,l)=h(l)*Nop(:,:,l) is the "Lagrange" h*N term
if subinfo.sparsemat
    Idmat = speye(nmax^n);
    zeromat = 0*Idmat;
else
    Idmat = eye(nmax^n);
    zeromat = zeros(nmax^n,nmax^n);
end
tOdag = cell(n,1);
Nop = cell(n,1);
for l = 1:n
    tOdag{l} = zeromat;
    Nop{l} = zeromat;
end
sumNop = zeromat;
sumhNop = zeromat;

% Identity of right size for the allowed occupancies for a single
% subsidiary boson
Id = eye(nmax,nmax);
if subinfo.sparsemat
    Id = sparse(Id);
end

% Occupancy values to use in the Hubbard type interaction term
diagOccs = diag(allowedOccs);
if subinfo.sparsemat
    diagOccs = sparse(diagOccs);
end

% loop over subsidiary bosons and create matrices for number and O operators
for l = 1:n
    % raising operator for l'th subsidiary boson; we accumulate C(l) into
    % the corner instead setting it to C(l) to correctly handle the
    % the nmax=2 (orbital+spin subsidiary or "spin-subsidiary") case.
    raise = diag(ones(nmax-1,1),-1);
    raise(1,end) = raise(1,end) + C(l);
    if subinfo.sparsemat
        raise = sparse(raise);
    end
    % bighop is l'th boson's Odag (raising) in full space scaled by
    %    hopping value t(l)
    % bignum is l'th boson's number operator in full space
    % this is accomplished by successive Kronecker products
    bighop = 1;
    bignum = 1;
    for k=1:n
        if k==l
            bighop = kron(bighop,t(l)*raise);
            bignum = kron(bignum,diagOccs);
        else
            bighop = kron(bighop,Id);
            bignum = kron(bignum,Id);
        end
    end
    tOdag{l} = bighop;
    Nop{l} = bignum;
    sumNop = sumNop + Nop{l};
    sumhNop = sumhNop + h(l)*Nop{l};   
end %l
clear Id raise diagOccs

% Now we build the Hamiltonian!
% Hopping parts (on and off terms)
sumtOdag = zeromat;
for l = 1:n
    sumtOdag = sumtOdag + tOdag{l};
end
Hhop = sumtOdag + sumtOdag';

% Hint is interaction part (shifted by "half-filling" values)
meanN = sum(meanOccs);
Hint = (U/2)*(sumNop-meanN*Idmat)^2;

% if there is a mismatch, we can't use the index tables in subinfo
% to compute the Up and J energy terms (most likely this is a call 
% from Csearch with U=Up=J=0 in such a case)
if n == subinfo.nsubspersite
    % if orbitally resolved, add Up term
    if subinfo.orbresolved
        Hint = Hint + ((Up-U)/2)*(sumNop-meanN*Idmat)^2;
        nio = size(subinfo.subbyorb{isite},1);
        for io = 1:nio
            idx = subinfo.subbyorb{isite}(io,:);
            Norb = zeromat;
            Navg = 0;
            for l = idx 
                Norb = Norb + Nop{l};
                Navg = Navg + meanOccs(l);
            end
            Hint = Hint - ((Up-U)/2)*(Norb-Navg*Idmat)^2;
        end
        clear io nio Norb idx Navg
    end
    % if spin resolved, add Hund's J term
    if subinfo.spinresolved
        for is = 1:2
            idx = subinfo.subbyspin{isite}(is,:);
            Nspin = zeromat;
            Navg = 0;
            for l = idx
                Nspin = Nspin + Nop{l};
                Navg = Navg + meanOccs(l);
            end
            Hint = Hint - (J/2)*(Nspin-Navg*Idmat)^2;
        end
        clear Nspin Navg l is
    end
end

% Sum up the three types of terms
% now Ham is sparse matrix
Ham = Hhop + sumhNop + Hint;

% Diagonalize (and ensure it is Hermitian for sure beforehand)
Ham = (Ham+Ham')/2;
if subinfo.sparsemat==1
    neig = max([ceil(nmax^n*subinfo.sparsefrac) 1]);
    [V,D] = eigs(Ham,neig,'smallestreal');
else
    [V,D] = eig(Ham);
end
energies = full(diag(D));
clear D

% Boltzmann weights
prob = exp(-(energies(:)-energies(1))/subinfo.kTsub);
Zpart = sum(prob);
prob = prob / Zpart;

% Compute number operator matrices in eigenbasis


% Compute the average of O operators, interaction energy, and occupancies
Oavg = zeros(1,n);
occs = zeros(1,n);
for l=1:n
    Oavg(l) = sum(prob.*diag(V'*(tOdag{l}/t(l))*V));
    Nopeig{l} = V'*Nop{l}*V;
    occs(l) = sum(prob.*diag(Nopeig{l}));
end
Eint = sum(prob.*diag(V'*Hint*V)); 

% list of configurations and their probabilities (thermal averaged)
configs = zeros(nmax^n,n);
for l = 1:n
    configs(:,l) = full(diag(Nop{l}));
end
pconfigs = diag(V*diag(prob)*V'); 

end % function
