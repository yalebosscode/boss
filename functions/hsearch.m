function [Oavg,h,Eint,subinfo] = hsearch(Occstomatch,Occsref,t,h,...
    occtol,subinfo,isite)
% function [Oavg,h,Eint,subinfo] = HSEARCH(Occstomatch,Occsref,t,h,...
%                                  occtol,subinfo,isite)
% Solves subsidiary problem on a site by adjusting Lagrange hl values until 
% subsidiary occupations match provided ones.  It is an outer driver for
% the actual subsidiary solver and uses Newton's method to match occupancies.
% This is solving a single site connected to a bath via hoppings t, 
% and n = # of subsidiaries on a site
% Input:
%   Occstomatch(1:n) = subsidiary occupancies to match
%   Occsref(1:n) = subsidiary occupancies that are reference values for
%                  interaction terms (the "double counting" values)
%   t(1:n) = hoppings onto site from bath (includes minus sign)
%   h(1:n) = input guess Lagrange values to start with
%   occtol = tolernace on maximum occupancy matching error
%   subinfo = needed subsidiary tables and indices are in this class
%   isite = site index of the subsidiary problem (for looking up in subinfo)
% Output:
%   Oavg(1:n) = <O> over the subsidiary modes 
%   h(1:n) = output (final) Lagrange hl values for subsidiary modes
%   Eint = Hubbard interaction energy from subsidiary problem <Hint>
%   subinfo = outgoing has various eigeninformation in it from HamSub call

global verbose;

n = subinfo.nsubspersite;
allowedOccs = subinfo.allowedOccs;
U = subinfo.U(isite);
Up = subinfo.Up(isite);
J = subinfo.J(isite);
kT = subinfo.kTsub;
C = subinfo.C(isite,:);
if verbose
    fprintf('--- hsearch: U=%g Up=%g J=%g n=%d occtol=%g kT=%g\n',...
        U,Up,J,n,occtol,kT);
    fprintf('allowedOccs = '); disp ([allowedOccs])
    fprintf('Occstomatch = '); disp ([Occstomatch])
    fprintf('Occsref = '); disp([Occsref])
    fprintf('C = '); disp([C])
    fprintf('t = '); disp([t])
end

if (length(allowedOccs)==2)
    %fprintf('*** Special case!  t->t/2 due to length(allowedOccs)==2\n');
    t = t/2;
end

% Newton's algorithm with scaling alpha
count = 0;
alpha = 1;
doccsold = ones(1,n);
occs = doccsold;
doccsdh = zeros(n,n);
while max(abs(Occstomatch-occs)) > occtol 
        
    % get current occupancies
    count = count+1;
    [Oavg,occs,Eint,Eigvals,Eigvecs,Eigprobs,configs,pconfigs,Neig,subinfo] = ...
        HamSub(U,Up,J,Occsref,t,h,C,n,subinfo,isite);
    doccs = abs(Occstomatch-occs);

    % Report on a pile of stuff if verbose
    if verbose
        fprintf('hsearch count=%d  max|doccold|=%.1e  max|docc|=%.1e  occtol=%.1e ',...
            count,max(doccsold),max(doccs),occtol);
        if verbose > 1 
            fprintf('\nh='); disp([h])
            fprintf('occs=     '); disp([occs])
            fprintf('occ2match='); disp([Occstomatch])
            fprintf('<O>='); disp([Oavg])
            fprintf('low-lying eigenvalues = ');
            neigprint = 5;
            if length(Eigvals) < 5
                neigprint = length(Eigvals);
            end
            disp([Eigvals(1:neigprint)])
        end
    end       
    
    % Derivative matrix doccs/dh for Newton's algorithm
    % We can do this analytically since the sub Hamiltonian has
    % only a linear coupling sum_l hl*Nl inside where h appear
    % so we can use perturbation theory to calculate
    % d<Ni>/dhj = beta*<Ni><Nj> + sum_{ab} D(a,b)*<a|Ni|b><b|Nj|a>
    % where D(a,b) = [p(a)-p(b)]/(E(a)-E(b)] and for the diagonal
    % or degenerate case it is the derivative p'(a) where p(a) is
    % the Boltzmann probability p(a)=exp(-beta*E(a))/Z
    beta = 1/subinfo.kTsub;
    neig = length(Eigvals);
    Ea = Eigvals*ones(1,neig);
    Eb = Ea';
    pa = Eigprobs*ones(1,neig);
    pb = pa';
    D = (pa-pb)./(Ea-Eb);
    inddeg = abs(Ea-Eb)*beta < 1e-6;
    D(inddeg) = -beta*pa(inddeg);
    for k=1:n
        NkD = Neig{k}.*D;
        for j=1:n
            doccsdh(j,k) = trace(Neig{j}*NkD);
        end
    end
    doccsdh = doccsdh + beta*occs'*occs;  % occs is row vector
    if verbose > 1
        fprintf('doccsdh=\n');
        disp([doccsdh])
    end
    clear beta neig Ea Eb pa pb D inddeg k j NkD

    % Newton's algorithm with scaling by alpha
    hshift = -alpha*inv(doccsdh')*([occs-Occstomatch]');
    hshift = hshift';    
    if verbose > 1
        fprintf('proposed hshift = '); disp([hshift])
    end
    
    % did we improve occupancies? if no, downscale hshift by 1/3
    improvedoccs = 1;
    if  max(doccs) > max(doccsold)
        hshift = hshift/3;
        improvedoccs = 0;
        if verbose > 1
            fprintf('occupancies worsened: alpha & hshift by scaled 1/3\n')
        end
    end
    
    % hshift too large? then scale down & alpha scaled by 1/3
    undermax = 1;
    for j=1:length(hshift)
        maxshift(j) = max(abs([U/10 t(j) h(j)]));
        if U>0 && abs(hshift(j)) > maxshift(j)
            if verbose > 1
                fprintf('hshift(%d)=%g > maxshift=%g\n',...
                    j,hshift(j),maxshift(j));
            end
            hshift(j) = alpha*maxshift(j)*sign(hshift(j));
            undermax = 0;
        end
    end
    
    % if our shift was not too large and we improved occupancies then
    % increase alpha towards 1
    if undermax && improvedoccs
        alpha = alpha*(2-alpha);
    % if we improved occupancies but went over maxima, don't change alpha
    elseif improvedoccs
        alpha = alpha;
    else % bad stuff happened: zap alpha by 1/3
        alpha = alpha/3;
    end   
    
    % too many iterations, whack alpha in the head to get it unstuck
    if mod(count,20)==0
        alpha = 0.25;
        if verbose
            fprintf('20 iterations passed... wacking alpha to 0.25\n')
        end
    end
    
    % shift h and save current occupancy differences for next cycle
    h = h + hshift;
    doccsold = doccs;
    if verbose
        fprintf('alpha = %g\n',alpha);
    end    
    
end % while loop of occupancies not matching




% final verbose report on electronic situation of subsidiaries
if verbose
   fprintf('h = '); disp([h])
   fprintf('Oavg = '); disp([Oavg])
   disp 'Subsidiary thermal averaged configurational distribution:'
   [~,isort] = sort(pconfigs,'descend');
   psum = 0;
   j = 0;
   while psum < 0.9
       j = j+1;
       pj = pconfigs(isort(j));
       psum = psum + pj;
       fprintf('  config [ ');
       for q=1:n
           fprintf('%d ',configs(isort(j),q));
       end
       fprintf(']  prob = %.3f   psum = %.3f\n',pj,psum);
   end
   fprintf('Low-lying subsidiary eigenstates:\n');
   neigprint = 3;
   if length(Eigvals) < 3
       neigprint = length(Eigvals);
   end   
   for j=1:neigprint
       fprintf('Eigenstate %d   E = %g   prob = %g\n',...
           j,Eigvals(j),Eigprobs(j))
       [~,isort] = sort(abs(Eigvecs(:,j)),'descend');
       psum = 0;
       c = 0;
       while psum < 0.9
           c = c+1;
           pb = abs(Eigvecs(isort(c),j))^2;
           psum = psum + pb;
           fprintf('  config [ ');
           for q=1:n
               fprintf('%d ',configs(isort(c),q));
           end
           fprintf('] <config|E> = %+.3f   prob = %.3f   psum = %.3f\n',...
               Eigvecs(isort(c),j),pb,psum)
       end
   end
end

% Store eigeninformation from the HamSub call for later work
subinfo.Eigvals{isite} = Eigvals;
subinfo.Eigvecs{isite} = Eigvecs;
subinfo.Eigprobs{isite} = Eigprobs;
subinfo.configs{isite} = configs;
subinfo.pconfigs{isite} = pconfigs;

end % function

