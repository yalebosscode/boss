% Solve self-consistent spinon+subsidiary problem in the absolutely
% simplest way possible: solve subsidiary then spinon then subsidiary then
% spinon then... This SCF loop is terminated when occupancy tolerance
% occtol is reached.

% We stop when change of electon count over every dorb is below occtol in
% magnitude from previous step.  However, to avoid numerical problems when
% this is being approached, the subsidiary+spinon problem must be solved
% more accurately.  So occtol is what is used by subsidiary+spinon while
% occtol_desired is used only in this loop for the termination criterion.
occtol_desired = occtol;
occtol = occtol / occtol_fac;
fprintf('Desired occtol = %g  ;  ',occtol_desired);
fprintf('Subsidiary problem solved to occtol = %g in SCF loop\n',occtol);

% Initialize Broyden.  Below, the nomenclature is 0 for current
% configuration and 1 for shifted configuration.  We are trying to solve
% fixed point iteration g(x)=x where x determines the state of the system.
% Specifically, g(x) represents the solve_sub_then_spinon activity: the
% subsidiary part has as input variables dcount (which it matches) and
% subinfo.tsub which are the hoppings of sub sites to the lattice; both can
% change so they form together our state variable x.  We pack them one
% after the other with reshape into a vector x. f(x)=g(x)-x is what we are
% trying to make zero based on Newton's method (with scaled step alpha for
% safety). We first initialize the Broyden. The while looks infinite but we
% break out inside  based on convergence.
alpha = 1;
ndcount = numel(dcount);
Jinv = -eye(2*ndcount);
scfstep = 1;
while 1 
    
    % pack dcount and tsub into x0 vector
    x0 = packdcounttsub(dcount,subinfo.tsub);
    % Solve subsidiary then spinon (i.e. go g(x0)) and get energies too 
    solve_sub_then_spinon
    Etot = Eband + Eint;   
    % pack new dcount & tsub into result g(x0)
    g0 = packdcounttsub(dcount,subinfo.tsub);
    % compute residual f(x0) which we want to make zero
    f0 = g0 - x0;
    % shift x based on inverse Jacobian
    x1 = x0 - alpha*Jinv*f0;
    % unpack shifted state into basic variables to then call g(x1)
    [dcount,subinfo.tsub] = unpackdcounttsub(x1);
    solve_sub_then_spinon; Etot = Eband + Eint;   
    % assemble g1 and compute shifted f(x1)
    g1 = packdcounttsub(dcount,subinfo.tsub);
    f1 = g1 - x1;
    % update inverse Jacobian based on change of f based on change of x
    dx = x1 - x0;
    df = f1 - f0;
    Jinv = Jinv + (1/(dx'*Jinv*df))*(dx-Jinv*df)*(dx'*Jinv);

    % maximum change of occupancies (dcount)
    maxoccdiff = max(abs(f1(1:ndcount)));
    
    % If we had a bad step or did 20 steps, reset alpha=1/3 and
    % inverse Jacobian to -identity to restart/kick Broyden.
    % If we had a good step, update alpha to iterate it towards 1
    % which is full Newton steps and best (theoretical) convergence.
    if scfstep > 1
        if maxoccdiff > maxoccdiffold || mod(scfstep,20)==0
            alpha = 1/3;
            Jinv = -eye(2*ndcount);
            disp 'Resetting alpha = 1/3 and Jinv = -I'
        else
            alpha = alpha*(2-alpha);
        end
     end

    % Report on progress (depending on verbosity level)
    % Most basic/compact is one line per iteration with basic stats
    if ~verbose
        fprintf('SCF step %3d : ',scfstep);
        fprintf('Etot= %.8f',Etot);
        if scfstep > 1
            fprintf('   dE= %+.1e',Etot-Etotold)
            fprintf('   max|Delta(dcount)|= %.1e   occtol= %.1e',...
                maxoccdiff,occtol_desired)
        end
        timestamp = clock;
        fprintf('  time=%d:%d:%.2f\n',timestamp(4:6));
    else
        fprintf('\n++++ Main SCF loop: scfstep = %d\n',scfstep)
        if scfstep > 1
            fprintf('max|Delta(dcount)|= %.1e   occtol= %.1e\n',...
                maxoccdiff,occtol);
        end
        fprintf('dcount=\n');
        disp([dcount])
        fprintf('Oavg= \n');
        disp([Oavg])
        Etot = Eband + Eint;
        fprintf('Eband= %g   Eint= %g   Etot= %.8f\n',Eband,Eint,Etot);
    end

    % are we done?
    if scfstep > 1 && maxoccdiff < occtol_desired
        break
    end
    
    % prepare for next iteration
    maxoccdiffold = maxoccdiff;
    Etotold = Etot;
    scfstep = scfstep + 1;
    
end
clear maxoccdiffold scfstep Etotold maxoccdiff
clear Jinv alpha f0 f1 g0 g1 x0 x1 df dx ndcount

% reset occtol to original value
occtol = occtol_desired;
clear occtol_desired dcountold Etotold

% Final report
fprintf('++++ Main SCF loop converged!\n')
fprintf('Final dcount =\n');
disp([dcount])
fprintf('Final Oavg = \n')
disp([Oavg])
fprintf('Etot = %.8f   Eband = %.8f   Eint = %.8f\n',Etot,Eband,Eint)
fprintf('\n')


% two helper functions only for this file to pack/unpack dcount & tsub
% into column vectors and back into matrices
function [x] = packdcounttsub(dcount,tsub)
    x = [reshape(dcount,[],1); reshape(tsub,[],1)];
end
function [dcount,tsub] = unpackdcounttsub(x)
    n = length(x)/2;
    dcount = reshape(x(1:n),2,[]); 
    tsub = reshape(x(n+1:end),2,[]);
end

