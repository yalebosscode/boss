function tsub = gettsub(orbs,corbs,porbs,tRij,rho)
% function tsub = GETTSUB(orbs,corbs,porbs,tRij,rho)
% Compute hoppings for subsidiary problem which are bare hoppings (tRij)
% multipled by p-d density matrix elements (rho) and summing over all
% p orbitals connected to each correated orbital.
% Input:
%   orbs = list of orbitals
%   corbs = correlated orbitals
%   porbs = uncorrelated orbitals
%   tRij(iR,1:norbs,1:norbs,spin) = hopping Hamiltonian
%   rho(iR,:,:,spin) = real-valued density matrix in real space
% Output:
%   tsub(is,icorb) = tRij*rhoij scaled hoppings for subsidiaries (pd model)

global verbose
if verbose
    fprintf('--- gettsub: computing subsidiary hoppings from tRij and rho\n');
end

ncorbs = length(corbs);
tsub = zeros(2,ncorbs);
for j = 1:ncorbs
    do = corbs(j);
    id = find( orbs == do );
    for po = porbs
        ip = find( orbs == po );
        for is=1:2
            tRidiprho = tRij(:,id,ip,is).*rho(:,id,ip,is);
            tsub(is,j) = tsub(is,j) + sum(tRidiprho);
        end
    end
end

end
