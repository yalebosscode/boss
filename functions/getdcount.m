function dcount = getdcount(Rlist,orbs,corbs,rho)
% function dcount = GETDCOUNT(Rlist,orbs,corbs,rho)
% Get the electron number count for each of the correlated orbitals
% and spin channels into dcount (from the supplied density matrix rho)
% Input:
%   Rlist(:,1:3) = list of R vectors 
%   orbs = list of orbitals
%   corbs = correlated orbitals
%   rho(iR,:,:,spin) = real-valued density matrix in real space
% Output:
%   dcount(spin,icorb) = electron count in the correlated orbitals

global verbose
if verbose
    fprintf('--- getdcount: getting dcount from rho\n');
end

ncorbs = length(corbs);
dcount = zeros(2,ncorbs);
% find R vectors that are zero (home unit cell) and for those
% entries of rho, identify diagonal elements corresponding to
% the correlated "d" orbitals
iR0 = find( sum(Rlist.^2,2) == 0 );
for j = 1:ncorbs
    do = corbs(j);
    idj = find( orbs == do );
    for is=1:2
        dcount(is,j) = dcount(is,j) + rho(iR0,idj,idj,is);
    end
end

end

