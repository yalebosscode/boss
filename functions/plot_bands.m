% Plots bare (Oavg=1) and scaled (Oavg) spinon band structures

% Recompute bare spinon bands and actual scaled spinon bands to get their
% respective chemical potentials mu1 and mu
O1 = ones(size(Oavg));
[eklist1,vklist1,rho1,mu1,tRijscale1,Eband1] = solvespinon_fixedN(...,
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,O1,subinfo.site_of_corb);
clear O1 rho1 Eband1
[eklist,vklist,rho,mu,tRijscale,Eband] = solvespinon_fixedN(...,
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,Oavg,subinfo.site_of_corb);

% some path in BZ: G to X to M to R to Z to G
nkaplot = 20;
uplot = [0:nkaplot-1]'/nkaplot;
rplot = [nkaplot:-1:1]'/nkaplot;
oplot = ones(size(uplot));
zplot = zeros(size(uplot));
kaplot = [uplot zplot zplot
          oplot uplot zplot
          oplot oplot uplot
          rplot rplot oplot
          zplot zplot rplot
          0 0 0];
kaplot = kaplot*pi;    
clear uplot zplot oplot rplot       
          
% compute band energies along the path
nkatotplot = size(kaplot,1);
ekplot1 = zeros(nkatotplot,norbs,2);
ekplot = ekplot1;
for j=1:nkatotplot
    for ispin=1:2
        [vk,ek1] = diagH_kspinon(kaplot(j,:)',Rlist,tRijscale1(:,:,:,ispin));
        [vk,ek] = diagH_kspinon(kaplot(j,:)',Rlist,tRijscale(:,:,:,ispin));
        ekplot1(j,:,ispin) = ek1;
        ekplot(j,:,ispin) = ek;
    end
end
clear vk ek1 ek nkatotplot tRijscale1

% plot the bands for each spin separately
figure(1)
clf
minmin = min([min(ekplot1(:))-mu1 min(ekplot(:))-mu])-5*gammaspectral;
maxmax = max([max(ekplot1(:))-mu1 max(ekplot(:))-mu])+5*gammaspectral;
for ispin=1:2
    subplot(1,2,ispin)
    hold on
    p1 = plot(ekplot1(:,:,ispin)-mu1,'-.','linewidth',2,'color',[.75 0 0]);
    hold on
    ps = plot(ekplot(:,:,ispin)-mu,'linewidth',3,'color',[0 0 .75]);
    set(gca,'fontsize',16)
    title(sprintf('Bare+spinon bands spin=%d',ispin))
    xlabel('k')
    if ispin == 1
        ylabel('E_{nk} - \mu (eV)')
        legend([p1(1),ps(1)],'bare','spinon','location','northwest')
    end
    %grid on
    set(gca,'xtick',[1 nkaplot+1 2*nkaplot+1 3*nkaplot+1 4*nkaplot+1 5*nkaplot+1]) 
    set(gca,'xticklabel',{'\Gamma','X','M','R','Z','\Gamma'})
    axis([1 5*nkaplot+1 minmin maxmax])
    set(gcf, 'Color', 'none');
end
clear minmin maxmax ekplot ekplot1 p1 ps




