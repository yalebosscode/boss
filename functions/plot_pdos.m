% Plots bare (Oavg=1) and scaled (Oavg) spinon PDOS

% Recompute bare spinon bands and actual scaled spinon bands to get their
% respective chemical potentials mu1 and mu
O1 = ones(size(Oavg));
[eklist1,vklist1,rho1,mu1,tRijscale1,Eband1] = solvespinon_fixedN(...,
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,O1,subinfo.site_of_corb);
clear O1 rho1 tRijscale1 Eband1
[eklist,vklist,rho,mu,tRijscale,Eband] = solvespinon_fixedN(...,
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,Oavg,subinfo.site_of_corb);

% Create energy axis
minmin = min([min(eklist1(:))-mu1 min(eklist(:))-mu]);
maxmax = max([max(eklist1(:))-mu1 max(eklist(:))-mu]);
eax = [minmin-5*gammaspectral:gammaspectral/4:maxmax+5*gammaspectral]';
neax = length(eax);

% compute Gaussian broadened pdos
pdos = zeros(neax,norbs,2);
pdos1 = pdos;
nktot = size(eklist,1);
for ispin=1:2
    for k=1:nktot
        for j=1:norbs
            Enk = eklist(k,j,ispin)-mu;
            unk = squeeze(vklist(k,:,j,ispin));
            pnk = exp(-(eax-Enk).^2/(2*gammaspectral^2))*abs(unk).^2;
            pdos(:,:,ispin) = pdos(:,:,ispin) + pnk;
            %
            Enk1 = eklist1(k,j,ispin)-mu1;
            unk1 = squeeze(vklist1(k,:,j,ispin));
            pnk1 = exp(-(eax-Enk1).^2/(2*gammaspectral^2))*abs(unk1).^2;
            pdos1(:,:,ispin) = pdos1(:,:,ispin) + pnk1;
        end
    end
end
pdos = pdos / sqrt(2*pi*gammaspectral^2)/ nktot;
pdos1 = pdos1 / sqrt(2*pi*gammaspectral^2)/ nktot;
clear ispin k j Enk unk pnk Enk1 unk1 pnk1

% find list of corbs and porbs in the master list for plotting
corblist = [];
porblist = [];
for j=corbs
    corblist = [corblist find(orbs==j)];
end
for j=porbs
    porblist = [porblist find(orbs==j)];
end

% Now plot pdos summed over corbs and porbs for each spin
figure(2)
clf
subplot(1,2,1)
hold on
for ispin=1:2
    isign = (-1)^(ispin+1);
    plot(isign*sum(pdos(:,corblist,ispin),2),eax,'-','linewidth',3,'color',[245 130 48]/255)
    plot(isign*sum(pdos(:,porblist,ispin),2),eax,'-','linewidth',3,'color',[0 130 200]/255)
   
end
maxabspdos = max(abs(sum(pdos(:,corblist,:),2)),[],'all');
maxabspdos = 1.2*maxabspdos;
xlabel('PDOS(E)')
ylabel('E-\mu (eV)')
set(gca,'fontsize',16)
title(sprintf('Spinon PDOS \\gamma=%g',gammaspectral))
legend('corbs','porbs','location','northeast')
grid on
axis([-maxabspdos maxabspdos min(eax) max(eax)])
%
subplot(1,2,2)
hold on
for ispin=1:2
    isign = (-1)^(ispin+1);
    plot(isign*sum(pdos1(:,corblist,ispin),2),eax,'-','linewidth',3,'color',[245 130 48]/255)
    plot(isign*sum(pdos1(:,porblist,ispin),2),eax,'-','linewidth',3,'color',[0 130 200]/255)
end
xlabel('PDOS(E)')
set(gca,'fontsize',16)
title(sprintf('Bare PDOS \\gamma=%g',gammaspectral))
legend('corbs','porbs','location','northeast')
grid on
axis([-maxabspdos maxabspdos min(eax) max(eax)])
 
% Now plot spinon pdos for corbs on each site separately
figure(3)
clf
for isite = 1:subinfo.nsites
    subplot(1,subinfo.nsites,isite)
    corbofsite = find(subinfo.site_of_corb(:) == isite);
    colvec = lines(length(corbofsite));
    for ispin=1:2
        isign = (-1)^(ispin+1);
        %plot(isign*sum(pdos(:,corbofsite,ispin),2),eax,'-','linewidth',3,'color',[245 130 48]/255)
        str = [];
        for ic = 1:length(corbofsite)
            str = sprintf('%s%s%d%s,',str,'''',corbofsite(ic),'''');
            plot(isign*pdos(:,corbofsite(ic),ispin),eax,'-','linewidth',2,'color',colvec(ic,:))
            hold on
        end
        eval(['legend(' str '''location'',''southeast'')'])
        clear str ic
    end
    xlabel('Spinon PDOS(E)')
    ylabel('E-\mu (eV)')
    set(gca,'fontsize',16)
    title(sprintf('corbs PDOS site %d',isite))
    grid on
    axis([-maxabspdos maxabspdos min(eax) max(eax)])  
end
clear corbofsite porblist corblist

