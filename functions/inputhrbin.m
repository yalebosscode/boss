function [tRij,Rlist] = inputhrbin(hrbinfile,orbs,corbs,site_of_corb,tijtol,zeroddflag)
%function [tRij,Rlist] = INPUTHRBIN(hrbinfile,orbs,corbs,site_of_corb,tijtol,zeroddflag)
% Reads binary hoppings file (converted from Wannier90 <base>_hr.dat) into
% tRij and Rlist.  Will remove imaginary parts of hoppings but will report
% on largest one. Removes all hoppings with absolute real part below tijtol.
% Also removes d-d (between corbs) between different sites if zeroddflag is
% set to 1; removing these will change your non-interacting bands away from
% the original Wannierization, how much it changes must be tested.
% Input:
%   hrbinfile = filename of binary hoppings data
%   orbs = indices of the Wannier functions to get hoppings for
%   corbs = indices of correlated (d) Wannier functions (subest of orbs)
%   site_of_corb = site index for each corb
%   tijtol = hoppings below this value in eV (in absolute value) are zeored
%   zeroddflag = 0 or 1 flag; 1 means remove d-d hoppings between different
%                sites; 0 means keep them
% Output:
%   tRij(iR,i,j) = <0i|H|Rj> = Wannier j in cell R hops to i in home cell 
%   Rlist(iR,1:3) = list of lattice vectors

% header info
fprintf('--- inputhrbin() ---\nReading binary file %s\n',hrbinfile)
fprintf('  orbs = ')
disp([orbs])
fprintf('  corbs = ')
disp([corbs])
fprintf('  site_of_corb = ')
disp([site_of_corb])
fprintf('  tijtol = %g eV\n',tijtol)

% read binary file & reshape into 7 column data set as in _hr.dat file
fprintf('  Opening file %s\n',hrbinfile);
[fid,errmsg] = fopen(hrbinfile);
if fid < 0
    disp(errmsg)
    stop
end
d0 = fread(fid,'double');
ntij = length(d0)/7;
d = reshape(d0,ntij,7);
clear d0
norbs = length(orbs);
fprintf('  tRij has %d entries ; norbs=%d ; %d/%d^2 = %g\n',...
    ntij,norbs,ntij,norbs,ntij/(norbs^2));

% get unique R vectors: this is just for reporting and will be done again
Rlist = unique(d(:,1:3),'rows');
nR = size(Rlist,1);
fprintf('  Found nR = %d unique R vectors\n',nR)

% report on maximum imaginary part of hopping and then drop them
fprintf('  Maximum imaginary tRij = %g eV ; zeroing Im(tRij)\n',...
    max(abs(d(:,7))))
d = d(:,1:6);

% drop all hoppings below tolerance tijtol
fprintf('  Removing hoppings below tijtol=%g eV (absolute value)\n',tijtol)
idx = find( abs(d(:,6)) >= tijtol );
d = d(idx,:);
ntij = size(d,1);
fprintf('  Now have %d hopping entries\n',ntij)

% get unique R vectors (again)
Rlist = unique(d(:,1:3),'rows');
nR = size(Rlist,1);
fprintf('  Found nR = %d unique R vectors\n',nR)


% drop all d-d (between corbs) hoppings between different sites if
% asked to do so
if zeroddflag == 1
    % first make a list of non-zero R vectors
    idxRnot0 = find( sum(abs(d(:,1:3)),2) > 0 );
    % loop over all d-d orbital pairs (corb pairs) and find which
    % site they belong to; if different sites, then zero that hopping
    maxzeroed = 0;
    for io = corbs
        for jo = corbs
            idxij = find( d(:,4)==io & d(:,5)==jo );
            isite = site_of_corb(find( corbs==io ));
            jsite = site_of_corb(find( corbs==jo ));
            % if both on same site, zero out cases with different R
            if isite == jsite
                idx = intersect(idxij,idxRnot0);
                % if on different sites, zero
            else
                idx = idxij;
            end
            m = d(idx,6);
            if max(abs(m)) > maxzeroed
                maxzeroed = max(abs(m));
            end
            %ibig = find(abs(m)>0.05);
            %disp([d(idx(ibig),:)])
            d(idx,6) = 0;
        end
    end
    fprintf('  Zeroed inter site d-d hoppings : |max| zeroed = %g eV\n',...
        maxzeroed)
end

% now extract hopping elements into t(R,i,j) (tRij) by finding
% matching R i j 
tRij = zeros(nR,norbs,norbs);
for iR = 1:nR
    R = Rlist(iR,:);
    idx = find( sum(abs(d(:,1:3)-ones(ntij,1)*R),2) == 0 );
    for j = 1:length(idx)
        o1 = find( d(idx(j),4) == orbs);
        o2 = find( d(idx(j),5) == orbs);
        tRij(iR,o1,o2) = d(idx(j),6);
    end
end

end

