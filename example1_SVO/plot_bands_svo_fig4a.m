% Plots bare (Oavg=1) and scaled (Oavg) spinon band structures

% Recompute bare spinon bands and actual scaled spinon bands to get their
% respective chemical potentials mu1 and mu
O1 = ones(size(Oavg));
[eklist1,vklist1,rho1,mu1,tRijscale1,Eband1] = solvespinon_fixedN(...,
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,O1);
clear O1 rho1 Eband1
[eklist,vklist,rho,mu,tRijscale,Eband] = solvespinon_fixedN(...,
    spinoninfo,orbs,corbs,porbs,tRij,Rlist,Oavg);

% some path in BZ: -X to G to X
nkaplot = 20;
uplot = [0:nkaplot-1]'/nkaplot;
rplot = [nkaplot:-1:1]'/nkaplot;
zplot = zeros(size(uplot));
kaplot = [-rplot zplot zplot
           uplot zplot zplot];
kaplot = kaplot*pi;    
clear uplot zplot rplot       
          
% compute band energies along the path
nkatotplot = size(kaplot,1);
ekplot1 = zeros(nkatotplot,norbs,2);
ekplot = ekplot1;
for j=1:nkatotplot
    for ispin=1:2
        [vk,ek1] = diagH_kspinon(kaplot(j,:)',Rlist,tRijscale1(:,:,:,ispin));
        [vk,ek] = diagH_kspinon(kaplot(j,:)',Rlist,tRijscale(:,:,:,ispin));
        ekplot1(j,:,ispin) = ek1;
        ekplot(j,:,ispin) = ek;
    end
end
clear vk ek1 ek nkatotplot tRijscale1

% plot the bands for each spin separately
figure(1)
clf
minmin = min([min(ekplot1(:))-mu1 min(ekplot(:))-mu]);
maxmax = max([max(ekplot1(:))-mu1 max(ekplot(:))-mu]);
ispin=1;
p1 = plot(ekplot1(:,:,ispin)-mu1,'g-.','linewidth',4);
hold on
ps = plot(ekplot(:,:,ispin)-mu,'r-','linewidth',5);
set(gca,'fontsize',16)
%title(sprintf('Bare+spinon bands spin=%d',ispin))
%xlabel('k')
if ispin == 1
    ylabel('E_{nk} - \mu (eV)')
    legend([p1(1),ps(1)],'bare','spinon','location','southeast')
end
grid on
set(gca,'xtick',[1 nkaplot+1 2*nkaplot+1])
set(gca,'xticklabel',{'X','\Gamma','X'})
axis([1 2*nkaplot+1 -2.5 1])
set(gca,'fontsize',24)
clear kaplot minmin maxmax ekplot ekplot1 p1 ps

set(gca,'color','k')
set(gca,'gridalpha',0.5)
set(gca,'gridcolor',[1 1 1])
set(gcf,'color','w')



