clear all

% orbs 1-5 are V 3d , 6-14 are O 2p
iproj = [1:5];
% scaling for projection circle sizes
sc = 150;
% Fermi energy from DFT NSCF output
EF = 12.2747;

kl=load('klist_dftbands');
kalist=kl(:,1:3);
kalist = kalist*2*pi;
hrdat = 'svo_hr.dat';
[enk,psink] = wanbands(hrdat,kalist);
enk = enk - EF;
pnk = abs(psink).^2;

load bands.dat
bands(:,2:end)=bands(:,2:end)-EF;

clf
hold on
plot(bands(:,1),enk,'g-','linewidth',3)%'markersize',4)
for j=1:size(enk,2)
    scatter(bands(:,1),enk(:,j),0.01+squeeze(sum(pnk(:,iproj,j),2))*sc,...
        'r','filled')
end
plot(bands(:,1),bands(:,2:end),'k','linewidth',1)
axis([0 1 -8 10])

grid
ylabel('$E_{nk}-E_F$','interpreter','latex')
set(gca,'fontsize',18)
set(gca,'xtick',bands([1 26 51],1))
set(gca,'xticklabel',{'R','\Gamma','M'})
title('SrVO_3 DFT bands + Wannier interpolation: V3d projections')
