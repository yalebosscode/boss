This directory contains the DFT and Wannierf90 files used to create the SrVO3 (SVO)
example.  They are provided as examples if you do not know (or need to remember)
how to do the DFT + Wannierization procedure.

The minimum thing you need to do here is to unzip the svo_hr.dat.gz file and
then run matlab on plotbands.m : it should work and plot the DFT bands from QE
(in black) and then on top the Wannierized bands (very small green dots) and
then the projections onto selected Wannier functions.  The matlab program
may in fact prove useful more generally when wanting to compare Wannierized
bands to actual DFT bands and to project bands onto Wannier orbitals to see
what is what.  An example output pdf of the matlab figure plotted is included.

The pseudopotentials are provided for completeness if you want to run QE 
on the input files in order to get everthing.  What you do is
1. QE on scf.in
2. QE on bands.in
3. QE on nscf.in
4. W90 with -pp option on svo.win
5. pw2wannier on pw2wan.in
6. W90 on svo.win

You then run the getbands.py script on bands.in > bands.dat to get the DFT bands

You need to also put a list of the bands.in k vectors used into klist_dftbands
so the Wannier bands can be computed on those k-points by plotbands.m

