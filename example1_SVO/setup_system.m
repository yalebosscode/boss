%----------------------------------------------------------------------
% The first part of setup sets the key parameters and model setups.
% The user must set these manually by editing the numbers below.


% Load/save state at start or end?
% If load_state = 1, then after initial setup and U=J=0 calculation,
% will read key state variables (h, Oavg, C, ...) from named file
load_state = 0;
load_state_file = 'finalstate.mat';
% if save_state = 1, will save final state variables to named file at end
save_state = 0;
save_state_file = 'finalstate.mat';


% The orbitals: who is who
% corbs is a list (row vector) supplied by the user saying which
% wannier orbitals are correlated ("d" orbitals in a p-d model)
% porbs is a list of the uncorrelated ("p") orbitals
% The orbitals can be in any order
corbs =[1:5];
porbs =[6:14];
orbs = [corbs porbs];  % all orbitals
norbs = length(orbs);  % number of total orbitals
fprintf('Correlated orbitals: corbs = ')
disp([corbs])
fprintf('Uncorrelated orbitals: porbs = ')
disp([porbs])
fprintf('Total number of orbitals: norbs = %d\n',norbs)


% Tight-binding parameters: where do they come from?
% the program only uses the hrbinfile assuming you have
% converted a standard text _hr.dat wannier90 output file
% to binary format (see appropriate simple matlab converter named ???)
% Up and down are separate files in principle but of course you can
% use the same filename to make them identical
hrbinfileup = 'SVO.bin';
hrbinfiledn = 'SVO.bin';
% After reading the tight-binding data, any hoppings below tijtol in
% magnitude (in eV) are dropped from list of hoppings.  Setting this
% to zero means all hoppings are retained
tijtol = 0.00;
% Whether we want to zero all d-d hoppings between different correlated
% sites: if set to 1, then all such dd hoppings are set to zero and
% the fact is reported; if not, then they are retained.  Zeroing them
% will change the non-interacting (U=Up=J=0) spinon bands to differ
% from the original Wannierization...
zeroddflag = 1;
% Occupancy matching tolerance betweeen subsidiaries and spinons
% also used in SCF loop for occupancy convergence
occtol = 1e-5;
% Factor by which required tolerance (internal occtol) is smaller than
% provided occtol: this is to makes that during the SCF loop the subsidiary
% problem is solved more accurately than the actual desired occtol
% so we don't get small numbers dancing around (numerical stability)
occtol_fac = 100;
fprintf('Will read hrbinfileup=%s and hrbinfiledn=%s with tijtol = %g eV ',...
    hrbinfileup,hrbinfiledn,tijtol)
fprintf('and zeroddflag = %d\n',zeroddflag)
fprintf('occtol = %g  occtol_fac = %g\n',...
    occtol,occtol_fac);


% Spinon information and tolerances
% density of k-point sampling along each sampled axis; 
spinoninfo.nk = [8 8 8];
% finite temperature in eV for Fermi-Dirac smearing of k-integrals
spinoninfo.kTspinon = 0.10; % finite smearing for spinon k integrals
% Total number of electrons (i.e., spinons) in the unit cell
spinoninfo.Ne = 19; % 2 x 3 x #Oxygen (2-) + d electrons (1 x # vanadium 4+) 
% Possible added p-d splitting (in eV): program will add this
% to onsite energy of "d" orbitals when building spinon Hamiltonian
spinoninfo.Dpd = 0;
% Tolerance on total number of electrons when finding mu
spinoninfo.Ntol = 1e-8;
fprintf('K-point sampling: nk=%d  kTspinon=%g eV  ',...
    spinoninfo.nk,spinoninfo.kTspinon);
fprintf('Ne=%d  Ntol=%g  Dpd=%g eV\n',...
    spinoninfo.Ne,spinoninfo.Ntol,spinoninfo.Dpd)


% Possible shift of on-site spinon energy for corb states by "big B
% magnetic fields" (symmetry breaking fields) that modify the spinon
% Hamiltonian on site energies away from the ones read in from file.  This
% is of the same size as h(spin,corb) or dcount. Here is a random
% initialization to uncomment if you want to start to minimize from some
% unbiased point, no guarantee it works for your system
%%%spinoninfo.Bfield = rand(2,length(corbs))-0.5;
% If you have no idea what this is or how to use it, just leave
% the line that zeros Bfield below intact.
spinoninfo.Bfield = zeros(2,length(corbs));

% do a Bfield=0 calculation before main calculation (1=yes, 0=no)
do_Bfield0_scf = 0;

% Subsidiary information and tolerances
% Here we setup data structures describing the subsidiary problem: namely
% which orbital+spin combinations belong to which subsidiary mode?  At
% present a very simple approach:  all sites (atoms) are identical, 
% all subsidiaries have an identical number of orbitals, all subs are either
% spin resolved (correspond to a spin channel) or sum over both spins.
% 
% Number of correlated sites, i.e., correlated atoms 
subinfo.nsites = 1;
% Number of subsidiary modes per site
subinfo.nsubspersite = 5;
% Allowed occupancies for each subsidiary mode
subinfo.allowedOccs = [0:2]; 
% Number of corr. orbitals per subsidiary mode
subinfo.ncorbspersub = 1; 
% Whether subsidiaries have spin index (1) or are summed over spin (0)
subinfo.spinresolved = 0;
% Decide if subsidiaries are orbitally resolved or not
if subinfo.ncorbspersub == 1
    subinfo.orbresolved = 1;
else
    subinfo.orbresolved = 0;
end
fprintf('nsites = %d\nnsubs/site = %d\nncorbs/sub = %d\n',...
    subinfo.nsites,subinfo.nsubspersite,subinfo.ncorbspersub)
fprintf('spinresolved = %d\n',subinfo.spinresolved)
fprintf('orbresolved = %d\n',subinfo.orbresolved)
fprintf('Allowed occupancies per subsidiary = ')
disp([subinfo.allowedOccs])


% Subsidiary Coulomb interaction parameters U, Up and J
% they are all row vectors in eV with values for each site
% U is  Coulomb repulsion for two electrons in same spatial orbital
% Up is Coulomb repulsion for two electrons in different spatial orbitals
% J is Hund's exchange for two electrons with same spin
% Traditional choice is Up = U-2*J if one has no other ideas of
% what to set Up to
subinfo.U = 12*ones(1,subinfo.nsites);
subinfo.J = subinfo.U*0;
subinfo.Up = subinfo.U-2*subinfo.J;
fprintf('U (eV) = ')
disp([subinfo.U])
fprintf('J (eV) = ')
disp([subinfo.J])
fprintf('Uprime (eV) = ')
disp([subinfo.Up])


% sparse matrix options diagonalizing subsidiary Hamiltonian
% subinfo.sparsemat = 1 : use sparse matrices ; 0 use dense (non sparse)
% if using sparse matrices, then subinfo.sparsefrac says what fraction
% of the lowest eigenvalues are to be computed (value between 0 and 1).
subinfo.sparsemat = 0;
subinfo.sparsefrac = 0.4;
if subinfo.sparsemat
    fprintf('Sparse matrices used for subs: %g%% of spectrum computed\n',...
        subinfo.sparsefrac*100)
end


% Subsidiary tolerances
% How close <O> is to 1 when solving U=J=0 problem for C and h
subinfo.Oavgtol = 1e-8;
% Finite temperature in eV for computing thermal expectations over 
% subsidiary eigenstates.  The idea is this should be tiny and basically
% zero temperature; the finite but small temperature is just to handle
% the unusual case of very nearly degenerate subsidiary ground and
% excited state so we properly average over them.
subinfo.kTsub = 0.1;
% these are numerical shifts for computing derivatives versus h or C
% during search routines using Newton (or Newton-like) algorithms
subinfo.dh = 1e-3;
subinfo.dC = 1e-3;
fprintf('Oavgtol = %g  kTsub = %g eV\n',...
    subinfo.Oavgtol,subinfo.kTsub)
fprintf('dh = %g    dC = %g\n',subinfo.dh,subinfo.dC);


% Should we perform a minimization of the total energy Etot versus
% symmetry breaking fields Bfield? If not set, then the mainprogram
% is just a one-shot calculation of the total energy via solution
% of spinon+subsidiary at whatever Bfield is provided in the main program.
% If set, it enters a big loop to minimze Etot over the components
% of Bfield. 0 = no, 1 = yes
minimize_Etot_over_Bfield = 0;
% The minimization is gradient descent with adjustable step size gamma.
% Minimization stops when energy change is below Etottol
% Gradient is computed via finite difference, dBfield is amount of shift 
% A key summary of minimization process is written to named file
% gammamin = smallest step size below which minimization terminates
% gammagrowfac = factor to grow gamma by each step when going down in Etot
% gammazapfac = factor to divide gamma by when gone uphill in Etot
miniminfo.Etottol = 1e-4; % in eV
miniminfo.gamma = 0.5; % initial value (before self adjustment)
miniminfo.dBfield = subinfo.dh;  % reasonable guess
miniminfo.logfile = 'minimization_log.txt';
miniminfo.gammamin = 1e-5; 
miniminfo.gammagrowfac = 1.1;
miniminfo.gammazapfac = 3;
if minimize_Etot_over_Bfield
    fprintf('Will minimize over Bfield, log written to file %s\n',...
        miniminfo.logfile);
    fprintf('Key minimization parameters:\n');
    fprintf('Etottol = %g eV , initial gamma = %g , dBfield = %g\n',...
        miniminfo.Etottol,miniminfo.gamma,miniminfo.dBfield);
    fprintf('gammamin = %g , gammagrowfrac = %g , gammazapfac = %g\n',...
        miniminfo.gammamin,miniminfo.gammagrowfac,miniminfo.gammazapfac);
end


% Plotting information
% Broadening for PDOS and other spectral quantities in eV
gammaspectral = 0.05;  % in eV


%----------------------------------------------------------------------
% Second part of setup builds key basic data structures and
% reads needed files to do so.  There are no more input paramters
% to set below this point.  Do not edit unless you need to.

% Sanity check that the number of corbs matches the subsidiary sizes and
% the various flags controlling how subsidiaries are partitioned
spinfac = 1+subinfo.spinresolved;
if subinfo.nsites*subinfo.nsubspersite*subinfo.ncorbspersub ~= ...
        length(corbs)*spinfac
    fprintf('We have a problem!\n')
    fprintf('Number of corbs*spinfac=%dx%d does ',length(corbs),spinfac)
    fprintf('not match nsites*nsubs/site*ncorb/sub=%dx%dx%d\n',...
        subinfo.nsites,subinfo.nsubspersite,subinfo.ncorbspersub)
    fprintf('Aborting\n\n')
    stop
end
clear spinfac

% Having passed the sanity test, we now build tables for each subsidiary that
% tell us which orbital+spin combinations belong to it on each site. This
% looks complicated because whether the subsidiaries include only one or both
% spins is told to us via a flag and the mapping is implicit, so we need
% ifs and various special handling.  However, the idea is simple: we go
% through the correlated orbital indices in a linear manner mapping them
% out in batches of size subinfo.ncorbspersub to the subsidiary modes going
% up in site and subsidiary count linearly.  Also builds an index table
% of corb to site.
corbcount = 1;
spincount = 1;
iclist = [1:subinfo.ncorbspersub]';
onelist = ones(size(iclist));
fprintf('Setting up subsidiary statetable information:\n');
for isite = 1:subinfo.nsites
    for isub = 1:subinfo.nsubspersite
        icorblist = corbcount + iclist - 1;
        if ~subinfo.spinresolved
            tab = [icorblist 1*onelist; icorblist 2*onelist];
            spincount = 2;
        else
            tab = [icorblist spincount*onelist];
        end
        subinfo.statetable{isite}{isub} = tab;
        spincount = spincount + 1;
        if spincount > 2
            spincount = 1;
            corbcount = corbcount + subinfo.ncorbspersub;
        end
        fprintf('  isite=%d  isub=%d  table(corb,spin) =\n',isite,isub)
        disp([subinfo.statetable{isite}{isub}])
        for ic = icorblist
            subinfo.site_of_corb(ic) = isite;
        end
    end
end
clear onelist iclist icorblist tab spincount corbcount ic

% Report the site index table contents: which site houses each orbtial
fprintf('Site indices for corbs:\n site(1:ncorbs) = ');
disp([subinfo.site_of_corb])

% If we have subsidiaries resolved by spin, make a table for each site showing
% which subsidiary are spin up or down
if subinfo.spinresolved
    fprintf('Spin resolution of subsidiaries:\n')
    for isite = 1:subinfo.nsites
        for is = 1:2
            list{is} = [];
        end
        for isub = 1:subinfo.nsubspersite
            is = subinfo.statetable{isite}{isub}(1,2);
            list{is} = [list{is} isub];
        end
        subinfo.subbyspin{isite} = [list{1}; list{2}];
        for is = 1:2
            fprintf('  isite=%d  spin=%d : local sub indices = ',isite,is);
            disp([subinfo.subbyspin{isite}(is,:)])
        end
    end
    clear is list isite isub
end

% If we have subs resolved by orbital, make a table on each site showing
% which subsidiary are mapped to which orbital on the site (local indices)
if subinfo.orbresolved
    fprintf('Orbital resolution of subsidiaries:\n')
    ncorbspersite = length(corbs)/subinfo.nsites;
    for isite = 1:subinfo.nsites
        subinfo.subbyorb{isite} = [];
        for io = 1:ncorbspersite
            list{io} = [];
        end
        for isub = 1:subinfo.nsubspersite
            io = subinfo.statetable{isite}{isub}(1,1);
            io = mod((io-1),ncorbspersite)+1;
            list{io} = [list{io} isub];
        end
        for io = 1:ncorbspersite
            subinfo.subbyorb{isite} = ...
                [subinfo.subbyorb{isite} ; list{io}];
        end
        for io = 1:ncorbspersite
            fprintf('  isite=%d : localicorb=%d, local subsidiary indices = ',...
                isite,io)
            disp([subinfo.subbyorb{isite}(io,:)])
        end
    end
    clear io ncorbpersite list
end

% flag special case of spin+orbital slave where we can compute
% so we can skip doing Csearch work and find C from formula
if subinfo.spinresolved && subinfo.orbresolved
    fprintf('Special case: subsidiary spin & orbital both true\n');
    fprintf('  Flagged to use formula to compute C in Csearch\n');
end

% Set subsidiary initial C numbers to 1
subinfo.C = ones(subinfo.nsites,subinfo.nsubspersite);
fprintf('Initial C all set to 1.  C(site,sub) =\n')
disp([subinfo.C])

% setup Oavg and h defaults
Oavg = ones(2,length(corbs));
subinfo.h = zeros(2,length(corbs));
fprintf('Initial Oavg(spin,corb) =\n')
disp([Oavg])
fprintf('Initital h(spin,corb) =\n')
disp([subinfo.h])

% set up zeored version of variables for initialization
subinfo.dcount0 = zeros(size(subinfo.h));

% Read Wannier data from wannier90 files to get tight-binding parameters
fprintf('Reading wannier data from %s for spin up\n',hrbinfileup)
[tRij(:,:,:,1),Rlist] = inputhrbin(hrbinfileup,orbs,corbs,...
    subinfo.site_of_corb,tijtol,zeroddflag);
fprintf('Reading wannier data from %s for spin dn\n',hrbinfiledn)
[tRij(:,:,:,2),Rlist] = inputhrbin(hrbinfiledn,orbs,corbs,...
    subinfo.site_of_corb,tijtol,zeroddflag);

% create spinon kgrid
spinoninfo.kalist = create_spinon_kgrid(spinoninfo.nk);
 
% We are done setting up!
fprintf('\n')
