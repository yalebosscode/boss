
This directory has example output from running the BoSS code on SrVO3.
The text output (via matlab diary command) are included for verbosity 0 and 1
as well as the PDOS and bands plot produced.

The system has 3 O and 1 V per unit cell.  With 3 O 2p per oxygen and 5 V 3d,
there are 14 Wannier orbitals per unit cell.  The Wannier generation procedure
for Wannier90 has orbitals 1-5 on V and 6-14 on the O 2p.  The subsidiary model in the
provided setup_system.m file has full orbital resolution but no spin resolution,
so 5 subsidiaries per V and each subsidiary can have occupancies 0, 1 or 2.

To run this you will need to first convert the _hr.dat file (after
ungzipping) to a binary SVO.bin with the convert_hrdat_to_bin.m
function and then use the provided setup_system.m file in this
directory.  It should produce the output provided.  The conversion
command to use in matlab is:

convert_hrdat_to_bin('SVO_hr.dat','SVO.bin','1.2')

Next, you need to put the SVO.bin file and the "setup_system.m" file
in this directory somewhere where the "mainprogram.m" and "functions/"
are located (copy them here or move this one directory up).  Then you
should be able to run "mainprogram" and generate the output and plots
provided in this directory.


