These are quantum espresso and wannier90 input files
for genereating the tight-binding description of SmNiO3
described by a 10-atom non-magnetic simulation.  A p-d model
is generated for O 2p and Ni 3d states.

The initital guess has been chosen carefully to align the 
Ni and O orbitals with appropriate local axes.

